/**
 * Concatenates two bytes together such that b1 is the hi part of the byte and b2 is the lo part.
 * @param {number} hi
 * @param {number} lo
 * @return {number}
 */
function concatBytes(hi, lo) {
    return (hi << 8) + lo;
}

exports.concatBytes = concatBytes;
