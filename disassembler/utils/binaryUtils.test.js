
const binaryUtils = require("./binaryUtils");

/* ===== Program counter ===== */

test("Concatenates bytes correctly", () => {
    expect(binaryUtils.concatBytes(0x02, 0x01)).toBe(0x201);
});
