
const disassembler = require("./disassembler");

/* ===== Program counter ===== */

/**
 * Disassembles a simple program (the following):
 *  lda #$10
 *  sta $10
 *  lda #$20
 *  adc $10
 */
test("Disassembles basic program", () => {

    const program = [0xa9, 0x10, 0x85, 0x10, 0xa9, 0x20, 0x65, 0x10];

    const results = disassembler.disassemble(program);

    expect(results).toEqual([{
        addressingMode: 'immediate',
        length: 2,
        tokens: ['lda', ' ', '#$10']
    }, {
        addressingMode: 'zero_page',
        length: 2,
        tokens: ['sta', ' ', '$10']
    }, {
        addressingMode: 'immediate',
        length: 2,
        tokens: ['lda', ' ', '#$20']
    }, {
        addressingMode: 'zero_page',
        length: 2,
        tokens: ['adc', ' ', '$10']
    }]);
});
