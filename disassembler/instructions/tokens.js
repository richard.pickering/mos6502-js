
exports.END_STATEMENT = '';
exports.COMMA = ',';
exports.X = 'X';
exports.Y = 'Y';
exports.A = 'A';
exports.PARENTHESIS_OPEN = '(';
exports.PARENTHESIS_CLOSE = ')';
exports.WHITESPACE = ' ';

/* Utility functions */

/**
 * Converts a number to a hex string.
 * @param {number} value
 * @return {string}
 */
exports.numericHex = function(value) {
    return '$' + value.toString(16);
};


/**
 * Converts a constant number to a hex string.
 * @param {number} value
 * @return {string}
 */
exports.numericConstantHex = function(value) {
    return '#$' + value.toString(16);
};

/* Instructions */
exports.ADC = 'adc';
exports.AND = 'and';
exports.ASL = 'asl';
exports.BCC = 'bcc';
exports.BCS = 'bcs';
exports.BEQ = 'beq';
exports.BIT = 'bit';
exports.BMI = 'bmi';
exports.BNE = 'bne';
exports.BPL = 'bpl';
exports.BRK = 'brk';
exports.BVC = 'bvc';
exports.BVS = 'bvs';
exports.CLC = 'clc';
exports.CLD = 'cld';
exports.CLI = 'cli';
exports.CLV = 'clv';
exports.CMP = 'cmp';
exports.CPX = 'cpx';
exports.CPY = 'cpy';
exports.DEC = 'dec';
exports.DEX = 'dex';
exports.DEY = 'dey';
exports.EOR = 'eor';
exports.INC = 'inc';
exports.INX = 'inx';
exports.INY = 'iny';
exports.JMP = 'jmp';
exports.JSR = 'jsr';
exports.LDA = 'lda';
exports.LDX = 'ldx';
exports.LDY = 'ldy';
exports.LSR = 'lsr';
exports.NOP = 'nop';
exports.ORA = 'ora';
exports.PHA = 'pha';
exports.PHP = 'php';
exports.PLA = 'pla';
exports.PLP = 'plp';
exports.ROL = 'rol';
exports.ROR = 'ror';
exports.RTI = 'rti';
exports.RTS = 'rts';
exports.SBC = 'sbc';
exports.SEC = 'sec';
exports.SED = 'sed';
exports.SEI = 'sei';
exports.STA = 'sta';
exports.STX = 'stx';
exports.STY = 'sty';
exports.TAX = 'tax';
exports.TAY = 'tay';
exports.TSX = 'tsx';
exports.TXA = 'txa';
exports.TXS = 'txs';
exports.TYA = 'tya';
