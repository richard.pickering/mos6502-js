const instructions = require("./instructions");

instructions.operations[0x69] = 'adc-immediate';
instructions.operations[0x65] = 'adc-zp';
instructions.operations[0x75] = 'adc-zpx';
instructions.operations[0x6D] = 'adc-abs';
instructions.operations[0x7D] = 'adc-abs-x';
instructions.operations[0x79] = 'adc-abs-y';
instructions.operations[0x61] = 'adc-indexed-indirect';
instructions.operations[0x71] = 'adc-indirect-indexed';

instructions.operations[0x29] = 'and-immediate';
instructions.operations[0x25] = 'and-zp';
instructions.operations[0x35] = 'and-zpx';
instructions.operations[0x2D] = 'and-abs';
instructions.operations[0x3D] = 'and-abs-x';
instructions.operations[0x39] = 'and-abs-y';
instructions.operations[0x21] = 'and-indexed-indirect';
instructions.operations[0x31] = 'and-indirect-indexed';

instructions.operations[0x0A] = 'asl-accumulator';
instructions.operations[0x06] = 'asl-zp';
instructions.operations[0x16] = 'asl-zpx';
instructions.operations[0x0E] = 'asl-abs';
instructions.operations[0x1E] = 'asl-abs-x';

instructions.operations[0x90] = 'bcc';
instructions.operations[0xB0] = 'bcs';
instructions.operations[0xF0] = 'beq';
instructions.operations[0x24] = 'bit-zp';
instructions.operations[0x2C] = 'bit-abs';
instructions.operations[0x30] = 'bmi';
instructions.operations[0xD0] = 'bne';
instructions.operations[0x10] = 'bpl';

instructions.operations[0x00] = 'brk';

instructions.operations[0x50] = 'bvc';
instructions.operations[0x70] = 'bvs';

instructions.operations[0x18] = 'clc';
instructions.operations[0xD8] = 'cld';
instructions.operations[0x58] = 'cli';
instructions.operations[0xB8] = 'clv';

instructions.operations[0xC9] = 'cmp-immediate';
instructions.operations[0xC5] = 'cmp-zp';
instructions.operations[0xD5] = 'cmp-zpx';
instructions.operations[0xCD] = 'cmp-abs';
instructions.operations[0xDD] = 'cmp-abs-x';
instructions.operations[0xD9] = 'cmp-abs-y';
instructions.operations[0xC1] = 'cmp-indexed-indirect';
instructions.operations[0xD1] = 'cmp-indirect-indexed';

instructions.operations[0xE0] = 'cpx-immediate';
instructions.operations[0xE4] = 'cpx-zp';
instructions.operations[0xEC] = 'cpx-abs';

instructions.operations[0xC0] = 'cpy-immediate';
instructions.operations[0xC4] = 'cpy-zp';
instructions.operations[0xCC] = 'cpy-abs';

instructions.operations[0xC6] = 'dec-zp';
instructions.operations[0xD6] = 'dec-zpx';
instructions.operations[0xCE] = 'dec-abs';
instructions.operations[0xDE] = 'dec-abs-x';

instructions.operations[0xCA] = 'dex';
instructions.operations[0x88] = 'dey';

instructions.operations[0x49] = 'eor-immediate';
instructions.operations[0x45] = 'eor-zp';
instructions.operations[0x55] = 'eor-zpx';
instructions.operations[0x4D] = 'eor-abs';
instructions.operations[0x5D] = 'eor-abs-x';
instructions.operations[0x59] = 'eor-abs-y';
instructions.operations[0x41] = 'eor-indexed-indirect';
instructions.operations[0x51] = 'eor-indirect-indexed';

instructions.operations[0xE6] = 'inc-zp';
instructions.operations[0xF6] = 'inc-zpx';
instructions.operations[0xEE] = 'inc-abs';
instructions.operations[0xFE] = 'inc-abs-x';

instructions.operations[0xE8] = 'inx';
instructions.operations[0xC8] = 'iny';

instructions.operations[0x4C] = 'jmp-abs';
instructions.operations[0x6C] = 'jmp-indirect';

instructions.operations[0x20] = 'jsr';

instructions.operations[0xA9] = 'lda-immediate';
instructions.operations[0xA5] = 'lda-zp';
instructions.operations[0xB5] = 'lda-zpx';
instructions.operations[0xAD] = 'lda-abs';
instructions.operations[0xBD] = 'lda-abs-x';
instructions.operations[0xB9] = 'lda-abs-y';
instructions.operations[0xA1] = 'lda-indexed-indirect';
instructions.operations[0xB1] = 'lda-indirect-indexed';

instructions.operations[0xA2] = 'ldx-immediate';
instructions.operations[0xA6] = 'ldx-zp';
instructions.operations[0xB6] = 'ldx-zpy';
instructions.operations[0xAE] = 'ldx-abs';
instructions.operations[0xBE] = 'ldx-abs-y';

instructions.operations[0xA0] = 'ldy-immediate';
instructions.operations[0xA4] = 'ldy-zp';
instructions.operations[0xB4] = 'ldy-zpx';
instructions.operations[0xAC] = 'ldy-abs';
instructions.operations[0xBC] = 'ldy-abs-x';

instructions.operations[0x4A] = 'lsr-accumulator';
instructions.operations[0x46] = 'lsr-zp';
instructions.operations[0x56] = 'lsr-zpx';
instructions.operations[0x4E] = 'lsr-abs';
instructions.operations[0x5E] = 'lsr-abs-x';

instructions.operations[0xEA] = 'nop';

instructions.operations[0x09] = 'ora-immediate';
instructions.operations[0x05] = 'ora-zeroPage';
instructions.operations[0x15] = 'ora-zeroPageX';
instructions.operations[0x0D] = 'ora-absolute';
instructions.operations[0x1D] = 'ora-absoluteX';
instructions.operations[0x19] = 'ora-absoluteY';
instructions.operations[0x01] = 'ora-indexedIndirect';
instructions.operations[0x11] = 'ora-indirectIndexed';

instructions.operations[0x48] = 'pha';
instructions.operations[0x08] = 'php';
instructions.operations[0x68] = 'pla';
instructions.operations[0x28] = 'plp';

instructions.operations[0x2A] = 'rol-acc';
instructions.operations[0x26] = 'rol-zp';
instructions.operations[0x36] = 'rol-zpx';
instructions.operations[0x2E] = 'rol-abs';
instructions.operations[0x3E] = 'rol-abs-x';

instructions.operations[0x6A] = 'ror-acc';
instructions.operations[0x66] = 'ror-zp';
instructions.operations[0x76] = 'ror-zpx';
instructions.operations[0x6E] = 'ror-abs';
instructions.operations[0x7E] = 'ror-abs-x';

instructions.operations[0x40] = 'rti';

instructions.operations[0x60] = 'rts';

instructions.operations[0xE9] = 'sbc-immediate';
instructions.operations[0xE5] = 'sbc-zp';
instructions.operations[0xF5] = 'sbc-zpx';
instructions.operations[0xED] = 'sbc-abs';
instructions.operations[0xFD] = 'sbc-abs-x';
instructions.operations[0xF9] = 'sbc-abs-y';
instructions.operations[0xE1] = 'sbc-indexed-indirect';
instructions.operations[0xF1] = 'sbc-indirect-indexed';

instructions.operations[0x38] = 'sec';
instructions.operations[0xF8] = 'sed';
instructions.operations[0x78] = 'sei';

// STA
instructions.operations[0x85] = 'sta-zp';
instructions.operations[0x95] = 'sta-zpx';
instructions.operations[0x8D] = 'sta-abs';
instructions.operations[0x9D] = 'sta-abs-x';
instructions.operations[0x99] = 'sta-abs-y';
instructions.operations[0x81] = 'sta-indexed-indirect';
instructions.operations[0x91] = 'sta-indirect-indexed';

// STX
instructions.operations[0x86] = 'stx-zp';
instructions.operations[0x96] = 'stx-zpy';
instructions.operations[0x8E] = 'stx-abs';

// STY
instructions.operations[0x84] = 'sty-zp';
instructions.operations[0x94] = 'sty-zpx';
instructions.operations[0x8C] = 'sty-abs';

instructions.operations[0xAA] = 'tax';
instructions.operations[0xA8] = 'tay';
instructions.operations[0xBA] = 'tsx';
instructions.operations[0x8A] = 'txa';
instructions.operations[0x9A] = 'txs';
instructions.operations[0x98] = 'tya';

test("Finds ADC", () => {
    expect(instructions.getOperation(0x69)).toBe('adc-immediate');
    expect(instructions.getOperation(0x65)).toBe('adc-zp');
    expect(instructions.getOperation(0x75)).toBe('adc-zpx');
    expect(instructions.getOperation(0x6D)).toBe('adc-abs');
    expect(instructions.getOperation(0x7D)).toBe('adc-abs-x');
    expect(instructions.getOperation(0x79)).toBe('adc-abs-y');
    expect(instructions.getOperation(0x61)).toBe('adc-indexed-indirect');
    expect(instructions.getOperation(0x71)).toBe('adc-indirect-indexed');
});

test("Finds AND", () => {
    expect(instructions.getOperation(0x29)).toBe('and-immediate');
    expect(instructions.getOperation(0x25)).toBe('and-zp');
    expect(instructions.getOperation(0x35)).toBe('and-zpx');
    expect(instructions.getOperation(0x2D)).toBe('and-abs');
    expect(instructions.getOperation(0x3D)).toBe('and-abs-x');
    expect(instructions.getOperation(0x39)).toBe('and-abs-y');
    expect(instructions.getOperation(0x21)).toBe('and-indexed-indirect');
    expect(instructions.getOperation(0x31)).toBe('and-indirect-indexed');
});

test("Finds ASL", () => {
    expect(instructions.getOperation(0x0A)).toBe('asl-accumulator');
    expect(instructions.getOperation(0x06)).toBe('asl-zp');
    expect(instructions.getOperation(0x16)).toBe('asl-zpx');
    expect(instructions.getOperation(0x0E)).toBe('asl-abs');
    expect(instructions.getOperation(0x1E)).toBe('asl-abs-x');
});

test("Finds BCC", () => {
    expect(instructions.getOperation(0x90)).toBe('bcc');
});

test("Finds BCS", () => {
    expect(instructions.getOperation(0xB0)).toBe('bcs');
});

test("Finds BEQ", () => {
    expect(instructions.getOperation(0xF0)).toBe('beq');
});

test("Finds BIT", () => {
    expect(instructions.getOperation(0x24)).toBe('bit-zp');
    expect(instructions.getOperation(0x2C)).toBe('bit-abs');
});

test("Finds BMI", () => {
    expect(instructions.getOperation(0x30)).toBe('bmi');
});

test("Finds BNE", () => {
    expect(instructions.getOperation(0xD0)).toBe('bne');
});

test("Finds BPL", () => {
    expect(instructions.getOperation(0x10)).toBe('bpl');
});

test("Finds BRK", () => {
    expect(instructions.getOperation(0x00)).toBe('brk');
});

test("Finds BVC", () => {
    expect(instructions.getOperation(0x50)).toBe('bvc');
});

test("Finds BVS", () => {
    expect(instructions.getOperation(0x70)).toBe('bvs');
});

test("Finds CLC", () => {
    expect(instructions.getOperation(0x18)).toBe('clc');
});

test("Finds CLD", () => {
    expect(instructions.getOperation(0xD8)).toBe('cld');
});

test("Finds CLI", () => {
    expect(instructions.getOperation(0x58)).toBe('cli');
});

test("Finds CLV", () => {
    expect(instructions.getOperation(0xB8)).toBe('clv');
});

test("Finds CMP", () => {
    expect(instructions.getOperation(0xC9)).toBe('cmp-immediate');
    expect(instructions.getOperation(0xC5)).toBe('cmp-zp');
    expect(instructions.getOperation(0xD5)).toBe('cmp-zpx');
    expect(instructions.getOperation(0xCD)).toBe('cmp-abs');
    expect(instructions.getOperation(0xDD)).toBe('cmp-abs-x');
    expect(instructions.getOperation(0xD9)).toBe('cmp-abs-y');
    expect(instructions.getOperation(0xC1)).toBe('cmp-indexed-indirect');
    expect(instructions.getOperation(0xD1)).toBe('cmp-indirect-indexed');
});

test("Finds CPX", () => {
    expect(instructions.getOperation(0xE0)).toBe('cpx-immediate');
    expect(instructions.getOperation(0xE4)).toBe('cpx-zp');
    expect(instructions.getOperation(0xEC)).toBe('cpx-abs');
});

test("Finds CPY", () => {
    expect(instructions.getOperation(0xC0)).toBe('cpy-immediate');
    expect(instructions.getOperation(0xC4)).toBe('cpy-zp');
    expect(instructions.getOperation(0xCC)).toBe('cpy-abs');
});

test("Finds DEC", () => {
    expect(instructions.getOperation(0xC6)).toBe('dec-zp');
    expect(instructions.getOperation(0xD6)).toBe('dec-zpx');
    expect(instructions.getOperation(0xCE)).toBe('dec-abs');
    expect(instructions.getOperation(0xDE)).toBe('dec-abs-x');
});

test("Finds DEX", () => {
    expect(instructions.getOperation(0xCA)).toBe('dex');
});

test("Finds DEY", () => {
    expect(instructions.getOperation(0x88)).toBe('dey');
});

test("Finds EOR", () => {
    expect(instructions.getOperation(0x49)).toBe('eor-immediate');
    expect(instructions.getOperation(0x45)).toBe('eor-zp');
    expect(instructions.getOperation(0x55)).toBe('eor-zpx');
    expect(instructions.getOperation(0x4D)).toBe('eor-abs');
    expect(instructions.getOperation(0x5D)).toBe('eor-abs-x');
    expect(instructions.getOperation(0x59)).toBe('eor-abs-y');
    expect(instructions.getOperation(0x41)).toBe('eor-indexed-indirect');
    expect(instructions.getOperation(0x51)).toBe('eor-indirect-indexed');
});

test("Finds INC", () => {
    expect(instructions.getOperation(0xE6)).toBe('inc-zp');
    expect(instructions.getOperation(0xF6)).toBe('inc-zpx');
    expect(instructions.getOperation(0xEE)).toBe('inc-abs');
    expect(instructions.getOperation(0xFE)).toBe('inc-abs-x');
});

test("Finds INX", () => {
    expect(instructions.getOperation(0xE8)).toBe('inx');
});

test("Finds INY", () => {
    expect(instructions.getOperation(0xC8)).toBe('iny');
});

test("Finds JMP", () => {
    expect(instructions.getOperation(0x4C)).toBe('jmp-abs');
    expect(instructions.getOperation(0x6C)).toBe('jmp-indirect');
});

test("Finds JSR", () => {
    expect(instructions.getOperation(0x20)).toBe('jsr');
});

test("Finds LDA", () => {
    expect(instructions.getOperation(0xA9)).toBe('lda-immediate');
    expect(instructions.getOperation(0xA5)).toBe('lda-zp');
    expect(instructions.getOperation(0xB5)).toBe('lda-zpx');
    expect(instructions.getOperation(0xAD)).toBe('lda-abs');
    expect(instructions.getOperation(0xBD)).toBe('lda-abs-x');
    expect(instructions.getOperation(0xB9)).toBe('lda-abs-y');
    expect(instructions.getOperation(0xA1)).toBe('lda-indexed-indirect');
    expect(instructions.getOperation(0xB1)).toBe('lda-indirect-indexed');
});

test("Finds LDX", () => {
    expect(instructions.getOperation(0xA2)).toBe('ldx-immediate');
    expect(instructions.getOperation(0xA6)).toBe('ldx-zp');
    expect(instructions.getOperation(0xB6)).toBe('ldx-zpy');
    expect(instructions.getOperation(0xAE)).toBe('ldx-abs');
    expect(instructions.getOperation(0xBE)).toBe('ldx-abs-y');
});

test("Finds LDY", () => {
    expect(instructions.getOperation(0xA0)).toBe('ldy-immediate');
    expect(instructions.getOperation(0xA4)).toBe('ldy-zp');
    expect(instructions.getOperation(0xB4)).toBe('ldy-zpx');
    expect(instructions.getOperation(0xAC)).toBe('ldy-abs');
    expect(instructions.getOperation(0xBC)).toBe('ldy-abs-x');
});

test("Finds LSR", () => {
    expect(instructions.getOperation(0x4A)).toBe('lsr-accumulator');
    expect(instructions.getOperation(0x46)).toBe('lsr-zp');
    expect(instructions.getOperation(0x56)).toBe('lsr-zpx');
    expect(instructions.getOperation(0x4E)).toBe('lsr-abs');
    expect(instructions.getOperation(0x5E)).toBe('lsr-abs-x');
});

test("Finds NOP", () => {
    expect(instructions.getOperation(0xEA)).toBe('nop');
});

test("Finds ORA", () => {
    expect(instructions.getOperation(0x09)).toBe('ora-immediate');
    expect(instructions.getOperation(0x05)).toBe('ora-zeroPage');
    expect(instructions.getOperation(0x15)).toBe('ora-zeroPageX');
    expect(instructions.getOperation(0x0D)).toBe('ora-absolute');
    expect(instructions.getOperation(0x1D)).toBe('ora-absoluteX');
    expect(instructions.getOperation(0x19)).toBe('ora-absoluteY');
    expect(instructions.getOperation(0x01)).toBe('ora-indexedIndirect');
    expect(instructions.getOperation(0x11)).toBe('ora-indirectIndexed');
});

test("Finds PHA", () => {
    expect(instructions.getOperation(0x48)).toBe('pha');
});

test("Finds PHP", () => {
    expect(instructions.getOperation(0x08)).toBe('php');
});

test("Finds PLA", () => {
    expect(instructions.getOperation(0x68)).toBe('pla');
});

test("Finds PLP", () => {
    expect(instructions.getOperation(0x28)).toBe('plp');
});

test("Finds ROL", () => {
    expect(instructions.getOperation(0x2A)).toBe('rol-acc');
    expect(instructions.getOperation(0x26)).toBe('rol-zp');
    expect(instructions.getOperation(0x36)).toBe('rol-zpx');
    expect(instructions.getOperation(0x2E)).toBe('rol-abs');
    expect(instructions.getOperation(0x3E)).toBe('rol-abs-x');
});

test("Finds ROR", () => {
    expect(instructions.getOperation(0x6A)).toBe('ror-acc');
    expect(instructions.getOperation(0x66)).toBe('ror-zp');
    expect(instructions.getOperation(0x76)).toBe('ror-zpx');
    expect(instructions.getOperation(0x6E)).toBe('ror-abs');
    expect(instructions.getOperation(0x7E)).toBe('ror-abs-x');
});

test("Finds RTI", () => {
    expect(instructions.getOperation(0x40)).toBe('rti');
});

test("Finds RTS", () => {
    expect(instructions.getOperation(0x60)).toBe('rts');
});

test("Finds SBC", () => {
    expect(instructions.getOperation(0xE9)).toBe('sbc-immediate');
    expect(instructions.getOperation(0xE5)).toBe('sbc-zp');
    expect(instructions.getOperation(0xF5)).toBe('sbc-zpx');
    expect(instructions.getOperation(0xED)).toBe('sbc-abs');
    expect(instructions.getOperation(0xFD)).toBe('sbc-abs-x');
    expect(instructions.getOperation(0xF9)).toBe('sbc-abs-y');
    expect(instructions.getOperation(0xE1)).toBe('sbc-indexed-indirect');
    expect(instructions.getOperation(0xF1)).toBe('sbc-indirect-indexed');
});

test("Finds SEC", () => {
    expect(instructions.getOperation(0x38)).toBe('sec');
});

test("Finds SED", () => {
    expect(instructions.getOperation(0xF8)).toBe('sed');
});

test("Finds SEI", () => {
    expect(instructions.getOperation(0x78)).toBe('sei');
});

test("Finds STA", () => {
    expect(instructions.getOperation(0x85)).toBe('sta-zp');
    expect(instructions.getOperation(0x95)).toBe('sta-zpx');
    expect(instructions.getOperation(0x8D)).toBe('sta-abs');
    expect(instructions.getOperation(0x9D)).toBe('sta-abs-x');
    expect(instructions.getOperation(0x99)).toBe('sta-abs-y');
    expect(instructions.getOperation(0x81)).toBe('sta-indexed-indirect');
    expect(instructions.getOperation(0x91)).toBe('sta-indirect-indexed');
});

test("Finds STX", () => {
    expect(instructions.getOperation(0x86)).toBe('stx-zp');
    expect(instructions.getOperation(0x96)).toBe('stx-zpy');
    expect(instructions.getOperation(0x8E)).toBe('stx-abs');
});

test("Finds STY", () => {
    expect(instructions.getOperation(0x84)).toBe('sty-zp');
    expect(instructions.getOperation(0x94)).toBe('sty-zpx');
    expect(instructions.getOperation(0x8C)).toBe('sty-abs');
});

test("Finds TAX", () => {
    expect(instructions.getOperation(0xAA)).toBe('tax');
});

test("Finds TAY", () => {
    expect(instructions.getOperation(0xA8)).toBe('tay');
});

test("Finds TSX", () => {
    expect(instructions.getOperation(0xBA)).toBe('tsx');
});

test("Finds TXA", () => {
    expect(instructions.getOperation(0x8A)).toBe('txa');
});

test("Finds TXS", () => {
    expect(instructions.getOperation(0x9A)).toBe('txs');
});

test("Finds TYA", () => {
    expect(instructions.getOperation(0x98)).toBe('tya');
});

test("Throws an error if an unrecognised op-code is provided", () => {
    expect(() => instructions.getOperation(0x100)).toThrowError("No operation defined for op-code 256");
});
