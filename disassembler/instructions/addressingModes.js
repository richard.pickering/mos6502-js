
const tokens = require("./tokens");
const binaryUtils = require("../utils/binaryUtils");

const ACCUMULATOR = 'accumulator';
const IMMEDIATE = 'immediate';
const RELATIVE = 'relative';
const IMPLIED = 'implied';
const ZERO_PAGE = 'zero_page';
const ZERO_PAGE_X = 'zero_page_x';
const ZERO_PAGE_Y = 'zero_page_y';
const ABSOLUTE = 'absolute';
const ABSOLUTE_X = 'absolute_x';
const ABSOLUTE_Y = 'absolute_y';
const INDIRECT = 'indirect';
const INDEXED_INDIRECT = 'indexed_indirect';
const INDIRECT_INDEXED = 'indirect_indexed';

exports.ACCUMULATOR = ACCUMULATOR;
exports.IMMEDIATE = IMMEDIATE;
exports.RELATIVE = RELATIVE;
exports.IMPLIED = IMPLIED;
exports.ZERO_PAGE = ZERO_PAGE;
exports.ZERO_PAGE_X = ZERO_PAGE_X;
exports.ZERO_PAGE_Y = ZERO_PAGE_Y;
exports.ABSOLUTE = ABSOLUTE;
exports.ABSOLUTE_X = ABSOLUTE_X;
exports.ABSOLUTE_Y = ABSOLUTE_Y;
exports.INDIRECT = INDIRECT;
exports.INDEXED_INDIRECT = INDEXED_INDIRECT;
exports.INDIRECT_INDEXED = INDIRECT_INDEXED;

exports.accumulator = function(current_offset, program, instruction) {

    return {
        length: 2,
        addressingMode: ACCUMULATOR,
        tokens: [instruction, tokens.WHITESPACE, tokens.A]
    };
};

exports.immediate = function(current_offset, program, instruction) {

    const parameter = tokens.numericConstantHex(program[current_offset + 1]);

    return {
        length: 2,
        addressingMode: IMMEDIATE,
        tokens: [instruction, tokens.WHITESPACE, parameter]
    };
};

exports.relative = function(current_offset, program, instruction) {

    const parameter = tokens.numericHex(program[current_offset + 1]);

    return {
        length: 2,
        addressingMode: RELATIVE,
        tokens: [instruction, tokens.WHITESPACE, parameter]
    };
};

exports.implied = function(current_offset, program, instruction) {

    return {
        length: 1,
        addressingMode: IMPLIED,
        tokens: [instruction]
    };
};

exports.zeroPage = function(current_offset, program, instruction) {

    const parameter = tokens.numericHex(program[current_offset + 1]);

    return {
        length: 2,
        addressingMode: ZERO_PAGE,
        tokens: [instruction, tokens.WHITESPACE, parameter]
    };
};

exports.zeroPageX = function(current_offset, program, instruction) {

    const parameter = tokens.numericHex(program[current_offset + 1]);

    return {
        length: 2,
        addressingMode: ZERO_PAGE_X,
        tokens: [instruction, tokens.WHITESPACE, parameter, tokens.COMMA, tokens.X]
    };
};

exports.zeroPageY = function(current_offset, program, instruction) {

    const parameter = tokens.numericHex(program[current_offset + 1]);

    return {
        length: 2,
        addressingMode: ZERO_PAGE_Y,
        tokens: [instruction, tokens.WHITESPACE, parameter, tokens.COMMA, tokens.Y]
    };
};

exports.absolute = function(current_offset, program, instruction) {

    const loByte = program[current_offset + 1];
    const hiByte = program[current_offset + 2];
    const overallValue = binaryUtils.concatBytes(hiByte, loByte);

    const parameter = tokens.numericHex(overallValue);

    return {
        length: 3,
        addressingMode: ABSOLUTE,
        tokens: [instruction, tokens.WHITESPACE, parameter]
    };
};

exports.absoluteX = function(current_offset, program, instruction) {

    const loByte = program[current_offset + 1];
    const hiByte = program[current_offset + 2];
    const overallValue = binaryUtils.concatBytes(hiByte, loByte);

    const parameter = tokens.numericHex(overallValue);

    return {
        length: 3,
        addressingMode: ABSOLUTE_X,
        tokens: [instruction, tokens.WHITESPACE, parameter, tokens.COMMA, tokens.X]
    };
};

exports.absoluteY = function(current_offset, program, instruction) {

    const loByte = program[current_offset + 1];
    const hiByte = program[current_offset + 2];
    const overallValue = binaryUtils.concatBytes(hiByte, loByte);

    const parameter = tokens.numericHex(overallValue);

    return {
        length: 3,
        addressingMode: ABSOLUTE_Y,
        tokens: [instruction, tokens.WHITESPACE, parameter, tokens.COMMA, tokens.Y]
    };
};

exports.indirect = function(current_offset, program, instruction) {

    const loByte = program[current_offset + 1];
    const hiByte = program[current_offset + 2];
    const overallValue = binaryUtils.concatBytes(hiByte, loByte);

    const parameter = tokens.numericHex(overallValue);

    return {
        length: 3,
        addressingMode: INDIRECT,
        tokens: [instruction, tokens.WHITESPACE, tokens.PARENTHESIS_OPEN, parameter, tokens.PARENTHESIS_CLOSE]
    };
};

exports.indexedIndirect = function(current_offset, program, instruction) {

    const parameter = tokens.numericHex(program[current_offset + 1]);

    return {
        length: 2,
        addressingMode: INDEXED_INDIRECT,
        tokens: [instruction, tokens.WHITESPACE, tokens.PARENTHESIS_OPEN, parameter, tokens.COMMA, tokens.X, tokens.PARENTHESIS_CLOSE]
    };
};

exports.indirectIndexed = function(current_offset, program, instruction) {

    const parameter = tokens.numericHex(program[current_offset + 1]);

    return {
        length: 2,
        addressingMode: INDIRECT_INDEXED,
        tokens: [instruction, tokens.WHITESPACE, tokens.PARENTHESIS_OPEN, parameter, tokens.PARENTHESIS_CLOSE, tokens.COMMA, tokens.Y]
    };
}
