
const addressingModes = require("./addressingModes");

test("Accumulator", () => {

    const result = addressingModes.accumulator(0, [0xAB], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.ACCUMULATOR);
    expect(result.tokens).toEqual(['ADC', ' ', 'A']);
});

test("Immediate", () => {

    const result = addressingModes.immediate(0, [0xAB, 0xCD], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.IMMEDIATE);
    expect(result.tokens).toEqual(['ADC', ' ', '#$cd']);
});

test("Relative", () => {

    const result = addressingModes.relative(0, [0xAB, 0xCD], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.RELATIVE);
    expect(result.tokens).toEqual(['ADC', ' ', '$cd']);
});

test("Implied", () => {

    const result = addressingModes.implied(0, [0xAB], 'CLC');

    expect(result.length).toBe(1);
    expect(result.addressingMode).toBe(addressingModes.IMPLIED);
    expect(result.tokens).toEqual(['CLC']);
});

test("Zero page", () => {

    const result = addressingModes.zeroPage(0, [0xAB, 0xCD], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.ZERO_PAGE);
    expect(result.tokens).toEqual(['ADC', ' ', '$cd']);
});

test("Zero page X", () => {

    const result = addressingModes.zeroPageX(0, [0xAB, 0xCD], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.ZERO_PAGE_X);
    expect(result.tokens).toEqual(['ADC', ' ', '$cd', ',', 'X']);
});

test("Zero page Y", () => {

    const result = addressingModes.zeroPageY(0, [0xAB, 0xCD], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.ZERO_PAGE_Y);
    expect(result.tokens).toEqual(['ADC', ' ', '$cd', ',', 'Y']);
});

test("Absolute", () => {

    const result = addressingModes.absolute(0, [0xAB, 0x01, 0x02], 'ADC');

    expect(result.length).toBe(3);
    expect(result.addressingMode).toBe(addressingModes.ABSOLUTE);
    expect(result.tokens).toEqual(['ADC', ' ', '$201']);
});

test("Absolute X", () => {

    const result = addressingModes.absoluteX(0, [0xAB, 0x01, 0x02], 'ADC');

    expect(result.length).toBe(3);
    expect(result.addressingMode).toBe(addressingModes.ABSOLUTE_X);
    expect(result.tokens).toEqual(['ADC', ' ', '$201', ',', 'X']);
});

test("Absolute Y", () => {

    const result = addressingModes.absoluteY(0, [0xAB, 0x01, 0x02], 'ADC');

    expect(result.length).toBe(3);
    expect(result.addressingMode).toBe(addressingModes.ABSOLUTE_Y);
    expect(result.tokens).toEqual(['ADC', ' ', '$201', ',', 'Y']);
});

test("Indirect", () => {

    const result = addressingModes.indirect(0, [0xAB, 0x02, 0x01], 'ADC');

    expect(result.length).toBe(3);
    expect(result.addressingMode).toBe(addressingModes.INDIRECT);
    expect(result.tokens).toEqual(['ADC', ' ', '(', '$102', ')']);
});

test("Indexed indirect", () => {

    const result = addressingModes.indexedIndirect(0, [0xAB, 0xCD], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.INDEXED_INDIRECT);
    expect(result.tokens).toEqual(['ADC', ' ', '(', '$cd', ',', 'X', ')']);
});

test("Indirect indexed", () => {

    const result = addressingModes.indirectIndexed(0, [0xAB, 0xCD], 'ADC');

    expect(result.length).toBe(2);
    expect(result.addressingMode).toBe(addressingModes.INDIRECT_INDEXED);
    expect(result.tokens).toEqual(['ADC', ' ', '(', '$cd', ')', ',', 'Y']);
});
