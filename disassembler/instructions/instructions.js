
const tokens = require("./tokens");
const addressing = require("./addressingModes");

const operations = {
    // ADC
    0x69: remap(addressing.immediate, tokens.ADC),
    0x65: remap(addressing.zeroPage, tokens.ADC),
    0x75: remap(addressing.zeroPageX, tokens.ADC),
    0x6D: remap(addressing.absolute, tokens.ADC),
    0x7D: remap(addressing.absoluteX, tokens.ADC),
    0x79: remap(addressing.absoluteY, tokens.ADC),
    0x61: remap(addressing.indexedIndirect, tokens.ADC),
    0x71: remap(addressing.indirectIndexed, tokens.ADC),
    // AND
    0x29: remap(addressing.immediate, tokens.AND),
    0x25: remap(addressing.zeroPage, tokens.AND),
    0x35: remap(addressing.zeroPageX, tokens.AND),
    0x2D: remap(addressing.absolute, tokens.AND),
    0x3D: remap(addressing.absoluteX, tokens.AND),
    0x39: remap(addressing.absoluteY, tokens.AND),
    0x21: remap(addressing.indexedIndirect, tokens.AND),
    0x31: remap(addressing.indirectIndexed, tokens.AND),
    // ASL
    0x0A: remap(addressing.accumulator, tokens.ASL),
    0x06: remap(addressing.zeroPage, tokens.ASL),
    0x16: remap(addressing.zeroPageX, tokens.ASL),
    0x0E: remap(addressing.absolute, tokens.ASL),
    0x1E: remap(addressing.absoluteX, tokens.ASL),
    0x90: remap(addressing.relative, tokens.BCC),
    0xB0: remap(addressing.relative, tokens.BCS),
    0xF0: remap(addressing.relative, tokens.BEQ),
    0x24: remap(addressing.zeroPage, tokens.BIT),
    0x2C: remap(addressing.absolute, tokens.BIT),
    0x30: remap(addressing.relative, tokens.BMI),
    0xD0: remap(addressing.relative, tokens.BNE),
    0x10: remap(addressing.relative, tokens.BPL),
    0x00: remap(addressing.implied, tokens.BRK),
    0x50: remap(addressing.relative, tokens.BVC),
    0x70: remap(addressing.relative, tokens.BVS),
    // Flag clear operations
    0x18: remap(addressing.implied, tokens.CLC),
    0xD8: remap(addressing.implied, tokens.CLD),
    0x58: remap(addressing.implied, tokens.CLI),
    0xB8: remap(addressing.implied, tokens.CLV),
    // CMP
    0xC9: remap(addressing.immediate, tokens.CMP),
    0xC5: remap(addressing.zeroPage, tokens.CMP),
    0xD5: remap(addressing.zeroPageX, tokens.CMP),
    0xCD: remap(addressing.absolute, tokens.CMP),
    0xDD: remap(addressing.absoluteX, tokens.CMP),
    0xD9: remap(addressing.absoluteY, tokens.CMP),
    0xC1: remap(addressing.indexedIndirect, tokens.CMP),
    0xD1: remap(addressing.indirectIndexed, tokens.CMP),
    // CPX
    0xE0: remap(addressing.immediate, tokens.CPX),
    0xE4: remap(addressing.zeroPage, tokens.CPX),
    0xEC: remap(addressing.absolute, tokens.CPX),
    // CPY
    0xC0: remap(addressing.immediate, tokens.CPY),
    0xC4: remap(addressing.zeroPage, tokens.CPY),
    0xCC: remap(addressing.absolute, tokens.CPY),
    // Decrement operations
    0xC6: remap(addressing.zeroPage, tokens.DEC),
    0xD6: remap(addressing.zeroPageX, tokens.DEC),
    0xCE: remap(addressing.absolute, tokens.DEC),
    0xDE: remap(addressing.absoluteX, tokens.DEC),
    0xCA: remap(addressing.implied, tokens.DEX),
    0x88: remap(addressing.implied, tokens.DEY),
    // EOR
    0x49: remap(addressing.immediate, tokens.EOR),
    0x45: remap(addressing.zeroPage, tokens.EOR),
    0x55: remap(addressing.zeroPageX, tokens.EOR),
    0x4D: remap(addressing.absolute, tokens.EOR),
    0x5D: remap(addressing.absoluteX, tokens.EOR),
    0x59: remap(addressing.absoluteY, tokens.EOR),
    0x41: remap(addressing.indexedIndirect, tokens.EOR),
    0x51: remap(addressing.indirectIndexed, tokens.EOR),
    // INC
    0xE6: remap(addressing.zeroPage, tokens.INC),
    0xF6: remap(addressing.zeroPageX, tokens.INC),
    0xEE: remap(addressing.absolute, tokens.INC),
    0xFE: remap(addressing.absoluteX, tokens.INC),
    // Increment operations
    0xE8: remap(addressing.implied, tokens.INX),
    0xC8: remap(addressing.implied, tokens.INY),
    // JMP
    0x4C: remap(addressing.absolute, tokens.JMP),
    0x6C: remap(addressing.indirect, tokens.JMP),
    // JSR
    0x20: remap(addressing.absolute, tokens.JSR),
    // LDA
    0xA9: remap(addressing.immediate, tokens.LDA),
    0xA5: remap(addressing.zeroPage, tokens.LDA),
    0xB5: remap(addressing.zeroPageX, tokens.LDA),
    0xAD: remap(addressing.absolute, tokens.LDA),
    0xBD: remap(addressing.absoluteX, tokens.LDA),
    0xB9: remap(addressing.absoluteY, tokens.LDA),
    0xA1: remap(addressing.indexedIndirect, tokens.LDA),
    0xB1: remap(addressing.indirectIndexed, tokens.LDA),
    // LDX
    0xA2: remap(addressing.immediate, tokens.LDX),
    0xA6: remap(addressing.zeroPage, tokens.LDX),
    0xB6: remap(addressing.zeroPageY, tokens.LDX),
    0xAE: remap(addressing.absolute, tokens.LDX),
    0xBE: remap(addressing.absoluteY, tokens.LDX),
    // LDY
    0xA0: remap(addressing.immediate, tokens.LDY),
    0xA4: remap(addressing.zeroPage, tokens.LDY),
    0xB4: remap(addressing.zeroPageX, tokens.LDY),
    0xAC: remap(addressing.absolute, tokens.LDY),
    0xBC: remap(addressing.absoluteX, tokens.LDY),
    // LSR
    0x4A: remap(addressing.accumulator, tokens.LSR),
    0x46: remap(addressing.zeroPage, tokens.LSR),
    0x56: remap(addressing.zeroPageX, tokens.LSR),
    0x4E: remap(addressing.absolute, tokens.LSR),
    0x5E: remap(addressing.absoluteX, tokens.LSR),
    0xEA: remap(addressing.implied, tokens.NOP),
    // ORA
    0x09: remap(addressing.immediate, tokens.ORA),
    0x05: remap(addressing.zeroPage, tokens.ORA),
    0x15: remap(addressing.zeroPageX, tokens.ORA),
    0x0D: remap(addressing.absolute, tokens.ORA),
    0x1D: remap(addressing.absoluteX, tokens.ORA),
    0x19: remap(addressing.absoluteY, tokens.ORA),
    0x01: remap(addressing.indexedIndirect, tokens.ORA),
    0x11: remap(addressing.indirectIndexed, tokens.ORA),
    // Stack push operations
    0x48: remap(addressing.implied, tokens.PHA),
    0x08: remap(addressing.implied, tokens.PHP),
    0x68: remap(addressing.implied, tokens.PLA),
    0x28: remap(addressing.implied, tokens.PLP),
    // ROL
    0x2A: remap(addressing.accumulator, tokens.ROL),
    0x26: remap(addressing.zeroPage, tokens.ROL),
    0x36: remap(addressing.zeroPageX, tokens.ROL),
    0x2E: remap(addressing.absolute, tokens.ROL),
    0x3E: remap(addressing.absoluteX, tokens.ROL),
    // ROR
    0x6A: remap(addressing.accumulator, tokens.ROR),
    0x66: remap(addressing.zeroPage, tokens.ROR),
    0x76: remap(addressing.zeroPageX, tokens.ROR),
    0x6E: remap(addressing.absolute, tokens.ROR),
    0x7E: remap(addressing.absoluteX, tokens.ROR),
    0x40: remap(addressing.implied, tokens.RTI),
    0x60: remap(addressing.implied, tokens.RTS),
    // SBC
    0xE9: remap(addressing.immediate, tokens.SBC),
    0xE5: remap(addressing.zeroPage, tokens.SBC),
    0xF5: remap(addressing.zeroPageX, tokens.SBC),
    0xED: remap(addressing.absolute, tokens.SBC),
    0xFD: remap(addressing.absoluteX, tokens.SBC),
    0xF9: remap(addressing.absoluteY, tokens.SBC),
    0xE1: remap(addressing.indexedIndirect, tokens.SBC),
    0xF1: remap(addressing.indirectIndexed, tokens.SBC),
    // Set processor status flag operations
    0x38: remap(addressing.implied, tokens.SEC),
    0xF8: remap(addressing.implied, tokens.SED),
    0x78: remap(addressing.implied, tokens.SEI),
    // STA
    0x85: remap(addressing.zeroPage, tokens.STA),
    0x95: remap(addressing.zeroPageX, tokens.STA),
    0x8D: remap(addressing.absolute, tokens.STA),
    0x9D: remap(addressing.absoluteX, tokens.STA),
    0x99: remap(addressing.absoluteY, tokens.STA),
    0x81: remap(addressing.indexedIndirect, tokens.STA),
    0x91: remap(addressing.indirectIndexed, tokens.STA),
    // STX
    0x86: remap(addressing.zeroPage, tokens.STX),
    0x96: remap(addressing.zeroPageY, tokens.STX),
    0x8E: remap(addressing.absolute, tokens.STX),
    // STY
    0x84: remap(addressing.zeroPage, tokens.STY),
    0x94: remap(addressing.zeroPageX, tokens.STY),
    0x8C: remap(addressing.absolute, tokens.STY),
    // Transfer instructions
    0xAA: remap(addressing.implied, tokens.TAX),
    0xA8: remap(addressing.implied, tokens.TAY),
    0xBA: remap(addressing.implied, tokens.TSX),
    0x8A: remap(addressing.implied, tokens.TXA),
    0x9A: remap(addressing.implied, tokens.TXS),
    0x98: remap(addressing.implied, tokens.TYA)
};

function remap(addressingMode, instructionName) {
    return {
        addressingMode: addressingMode,
        instructionName: instructionName
    }
}

/**
 * Returns the operation for the given op-code.
 * @param {number} opCode
 * @return {*}
 */
function getOperation(opCode) {

    if (operations[opCode] === undefined) {
        throw new Error(`No operation defined for op-code ${opCode}`);
    }

    return operations[opCode];
}

exports.getOperation = getOperation;
exports.operations = operations;
