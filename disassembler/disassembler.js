
const instructions = require("./instructions/instructions.js");

/**
 * Disassembles the parameterised program and provides back an array of tokens.
 * @param {Array} program
 * @return {Array}
 */
exports.disassemble = function(program) {

    const results = [];
    let counter = 0;
    while (counter < program.length) {

        const opCode = program[counter];

        const instruction = instructions.getOperation(opCode);

        const result = instruction.addressingMode(counter, program, instruction.instructionName);

        results.push(result);

        counter += result.length;
    }

    return results;
};
