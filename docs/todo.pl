#!/usr/bin/perl

sub format_result {

	my $value = shift;

	$value =~ s/\n###(.+\n)//gm;

	return $1;
}

open(FILEHANDLE, "<docs/Instructions.md");

my $preceding_lines = "";
my $counter = 0;

while (<FILEHANDLE>) {
	if (/__NO__/) {
		my $formatted = format_result($preceding_lines);
		print($formatted);
		$preceding_lines = "";
		$counter++;
	} else {
		$preceding_lines .= $_;
	}
}

close(FILEHANDLE);

print("\n$counter instructions left to implement.\n\n");

