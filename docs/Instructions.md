# NES Emulator - MOS 6502 Emulation

## Instructions to support

This section discusses the difficulty of implementing each instruction and is an initial finger in the air estimation of difficulty.  The advantage of providing this is for planning when these instructions should be developed and added to the code base of the NES Emulator.

### ADC - Add with Carry

Difficulty: __HARD__
Complete: __YES__

### AND - Logical AND

Difficulty: __HARD__
Complete: __YES__

### ASL - Arithmetic Shift Left

Difficulty: __MEDIUM__
Complete: __YES__

### BCC - Branch if Carry Clear

Difficulty: __EASY__
Complete: __YES__

### BCS - Branch if Carry Set

Difficulty: __EASY__
Complete: __YES__

### BEQ - Branch if Equal

Difficulty: __EASY__
Complete: __YES__

### BIT - Bit Test

Difficulty: __MEDIUM__
Complete: __YES__

### BMI - Branch if Minus

Difficulty: __EASY__
Complete: __YES__

### BNE - Branch if Not Equal

Difficulty: __EASY__
Complete: __YES__

### BPL - Branch if Positive

Difficulty: __EASY__
Complete: __YES__

### BRK - Force Interrupt

Difficulty: __HARD__
Complete: __YES__

### BVC - Branch if Overflow Clear 

Difficulty: __EASY__
Complete: __YES__

### BVS - Branch if Overflow Set

Difficulty: __EASY__
Complete: __YES__

### CLC - Clear Carry Flag

Difficulty: __EASY__
Complete: __YES__

### CLD - Clear Decimal Mode

Difficulty: __EASY__
Complete: __YES__

### CLI - Clear Interrupt Disable

Difficulty: __EASY__
Complete: __YES__

### CLV - Clear Overflow Flag

Difficulty: __EASY__
Complete: __YES__

### CMP - Compare

Difficulty: __HARD__
Complete: __YES__

### CPX - Compare X Register

Difficulty: __MEDIUM__
Complete: __YES__

### CPY - Compare Y Register

Difficulty: __MEDIUM__
Complete: __YES__

### DEC - Decrement Memory

Difficulty: __MEDIUM__
Complete: __YES__

### DEX - Decrement X Register

Difficulty: __EASY__
Complete: __YES__

### DEY - Decrement Y Register

Difficulty: __EASY__
Complete: __YES__

### EOR - Exclusive OR

Difficulty: __HARD__
Complete: __YES__

### INC - Increment Memory

Difficulty: __MEDIUM__
Complete: __YES__

### INX - Increment X Register

Difficulty: __EASY__
Complete: __YES__

### INY - Increment Y Register

Difficulty: __EASY__
Complete: __YES__

### JMP - Jump

Difficulty: __MEDIUM__
Complete: __YES__

### JSR - Jump to subroutine

Difficulty: __MEDIUM__
Complete: __YES__

### LDA - Load Accumulator

Difficulty: __HARD__
Complete: __YES__

### LDX - Load X Register

Difficulty: __HARD__
Complete: __YES__

### LDY - Load Y Register

Difficulty: __HARD__
Complete: __YES__

### LSR - Logical Shift Right

Difficulty: __MEDIUM__
Complete: __YES__

### NOP - No operation

Difficulty: __EASY__
Complete: __YES__

### ORA - Logical Inclusive OR

Difficulty: __HARD__
Complete: __YES__

### PHA - Push Accumulator

Difficulty: __EASY__
Complete: __YES__

### PHP - Push processor status

Difficulty: __EASY__
Complete: __YES__

### PLA - Pull Accumulator

Difficulty: __EASY__
Complete: __YES__

### PLP - Pull Processor Status

Difficulty: __EASY__
Complete: __YES__

### ROL - Rotate Left

Difficulty: __HARD__
Complete: __YES__

### ROR - Rotate Right

Difficulty: __HARD__
Complete: __YES__

### RTI - Return from Interrupt

Difficulty: __MEDIUM__
Complete: __YES__

### RTS - Return from Subroutine

Difficulty: __EASY__
Complete: __YES__

### SBC - Subtract with carry

Difficulty: __HARD__
Complete: __YES__

### SEC - Set Carry flag

Difficulty: __EASY__
Complete: __YES__

### SED - Set Decimal Flag

Difficulty: __EASY__
Complete: __YES__

### SEI - Set Interrupt Disable

Difficulty: __EASY__
Complete: __YES__

### STA - Store Accumulator

Difficulty: __HARD__
Complete: __YES__

### STX - Store X Register

Difficulty: __EASY__
Complete: __YES__

### STY - Store Y Register

Difficulty: __EASY__
Complete: __YES__

### TAX - Transfer Accumulator to X

Difficulty: __EASY__
Complete: __YES__

### TAY - Transfer Accumulator to Y

Difficulty: __EASY__
Complete: __YES__

### TSX - Transfer Stack Pointer to X

Difficulty: __EASY__
Complete: __YES__

### TXA - Transfer X to Accumulator

Difficulty: __EASY__
Complete: __YES__

### TXS - Transfer X to Stack Pointer

Difficulty: __EASY__
Complete: __YES__

### TYA - Transfer Y to accumulator

Difficulty: __EASY__
Complete: __YES__




