/*
 * Forces the generation of an interrupt request, setting the program counter to the values held at 0xFFFE/0xFFFF and
 * setting the break flag in the processor status register.  Pushes the old program counter and processor status values
 * to the stack.
 */

const result = {
    cycles: 7,
    payloadLength: 1
};

/**
 * Triggers the break interrupt. This causes the program counter to be stored on the stack and the processor status.
 * The break flag is then set in the processor status and the program counter is set to the contents of memory locations 0xFFFE/0xFFFF
 * to transfer program control over to the interrupt vector selected for this type of interrupt.
 * @param emulator
 */
function triggerInterrupt(emulator) {

    emulator.pushToStack(emulator.registers.programCounterHigh);
    emulator.pushToStack(emulator.registers.programCounterLow);
    emulator.pushToStack(emulator.processorStatus.value);

    const vectorLowAddress = 0xFFFE;
    const vectorHighAddress = 0xFFFF;
    emulator.processorStatus.break = true;

    emulator.registers.programCounterLow = emulator.memory.getByte(vectorLowAddress);
    emulator.registers.programCounterHigh = emulator.memory.getByte(vectorHighAddress);
}

exports.operation = function(emulator) {

    emulator.advanceExecutionPointer();

    triggerInterrupt(emulator, false);

    return result;
};

exports.name = 'BRK';
