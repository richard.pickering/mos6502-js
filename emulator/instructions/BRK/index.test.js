
const brk = require("./index");

const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        advanceExecutionPointer: jest.fn(),
        processorStatus: {
            break: false,
            value: 0
        },
        memory: new memory.Memory(),
        registers: {
            programCounter: 0x10,
            programCounterHigh: 0,
            programCounterLow: 0x10
        },
        pushToStack: jest.fn()
    };
});

test("Name is BRK", () => {
    expect(brk.name).toBe('BRK');
});

test("Operation succeeds", () => {

    emulator.processorStatus.value = 0b11110000;
    emulator.memory.setByte(0xFFFE, 0x01);
    emulator.memory.setByte(0xFFFF, 0x02);

    expect(emulator.processorStatus.break).toBeFalsy();

    const result = brk.operation(emulator);

    expect(result.cycles).toBe(7);
    expect(result.payloadLength).toBe(1);

    expect(emulator.advanceExecutionPointer).toBeCalled();

    expect(emulator.registers.programCounterLow).toBe(0x01);
    expect(emulator.registers.programCounterHigh).toBe(0x02);

    expect(emulator.processorStatus.break).toBeTruthy();

    expect(emulator.pushToStack).toHaveBeenNthCalledWith(1, 0x0);
    expect(emulator.pushToStack).toHaveBeenNthCalledWith(2, 0x10);
    expect(emulator.pushToStack).toHaveBeenNthCalledWith(3, 0b11110000);
});
