/*
 * Pulls the program counter from the stack.
 */

const result = {
    cycles: 6,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.programCounterLow = emulator.pullFromStack();
    emulator.registers.programCounterHigh = emulator.pullFromStack();

    return result;
};

exports.name = 'RTS';
