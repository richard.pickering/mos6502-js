
const rts = require("./index");
const emulatorModule = require("../../emulator");

let emulator = {};

beforeEach(() => {
    emulator = new emulatorModule.Emulator();
});

test("Name is RTS", () => {
    expect(rts.name).toBe('RTS');
});

test("Pulls the program counter from the stack", () => {

    emulator.pushToStack(0x17);
    emulator.pushToStack(0x21);

    const result = rts.operation(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(1);

    expect(emulator.registers.programCounter).toBe(0x1721);
});
