
const sta = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            y: 0,
            x: 0,
            accumulator: 0,
            programCounter: 0
        },
        advanceExecutionPointer: jest.fn(),
        memory: new memory.Memory()
    };
});

test("Name is STA", () => {
    expect(sta.name).toBe('STA');
});

test("Zero page addressing succeeds", () => {

    emulator.memory.setByte(1, 128);
    emulator.registers.accumulator = 192;

    const result = sta.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(128)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.memory.setByte(1, 123);
    emulator.registers.x = 5;
    emulator.registers.accumulator = 192;

    const result = sta.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(128)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.memory.setByte(1, 0x4);
    emulator.memory.setByte(2, 0x2);

    emulator.registers.accumulator = 192;

    const result = sta.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.memory.getByte(0x204)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.memory.setByte(1, 0x3);
    emulator.memory.setByte(2, 0x2);

    emulator.registers.accumulator = 192;
    emulator.registers.x = 0x1;

    const result = sta.absoluteX(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.memory.getByte(0x204)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute Y addressing succeeds", () => {

    emulator.memory.setByte(1, 0x3);
    emulator.memory.setByte(2, 0x2);

    emulator.registers.accumulator = 192;
    emulator.registers.y = 0x1;

    const result = sta.absoluteY(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.memory.getByte(0x204)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Indexed indirect addressing succeeds", () => {

    emulator.memory.setByte(1, 0x30);

    emulator.memory.setByte(0x40, 0x01);
    emulator.memory.setByte(0x41, 0x02);

    emulator.registers.accumulator = 192;
    emulator.registers.x = 0x10;

    const result = sta.indexedIndirect(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(0x201)).toBe(192);
});

test("Indirect indexed addressing succeeds", () => {

    emulator.memory.setByte(1, 0x40);

    emulator.memory.setByte(0x40, 0x01);
    emulator.memory.setByte(0x41, 0x02);

    emulator.registers.accumulator = 192;
    emulator.registers.y = 0xFF;

    const result = sta.indirectIndexed(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(0x300)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});
