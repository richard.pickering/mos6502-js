/*
 * Stores the value of the accumulator register at a specified address in memory.
 */

const addressing = require("../../utils/addressing");

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteXResult = {
    cycles: 5,
    payloadLength: 3
};

const absoluteYResult = {
    cycles: 5,
    payloadLength: 3
};

const indexedIndirectResult = {
    cycles: 6,
    payloadLength: 2
};

const indirectIndexedResult = {
    cycles: 6,
    payloadLength: 2
};

function zeroPage(emulator) {

    const zeroPageAddress = addressing.getZeroPageAddress(emulator);
    const accumulator = emulator.registers.accumulator;

    emulator.memory.setByte(zeroPageAddress, accumulator);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const zeroPageAddress = addressing.getZeroPageXAddress(emulator);
    const accumulator = emulator.registers.accumulator;

    emulator.memory.setByte(zeroPageAddress, accumulator);

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const absoluteAddress = addressing.getAbsoluteAddress(emulator);
    const accumulator = emulator.registers.accumulator;

    emulator.memory.setByte(absoluteAddress, accumulator);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const absoluteAddress = addressing.getAbsoluteXAddress(emulator).address;
    const accumulator = emulator.registers.accumulator;

    emulator.memory.setByte(absoluteAddress, accumulator);

    emulator.advanceExecutionPointer(3);

    return absoluteXResult;
}

function absoluteY(emulator) {

    const absoluteAddress = addressing.getAbsoluteYAddress(emulator).address;
    const accumulator = emulator.registers.accumulator;

    emulator.memory.setByte(absoluteAddress, accumulator);

    emulator.advanceExecutionPointer(3);

    return absoluteYResult;
}

function indexedIndirect(emulator) {

    const address = addressing.getIndexedIndirectAddress(emulator);
    const accumulator = emulator.registers.accumulator;

    emulator.memory.setByte(address, accumulator);

    emulator.advanceExecutionPointer(2);

    return indexedIndirectResult;
}

function indirectIndexed(emulator) {

    const address = addressing.getIndirectIndexedAddress(emulator).address;
    const accumulator = emulator.registers.accumulator;

    emulator.memory.setByte(address, accumulator);

    emulator.advanceExecutionPointer(2);

    return indirectIndexedResult;
}

exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.absoluteY = absoluteY;
exports.indexedIndirect = indexedIndirect;
exports.indirectIndexed = indirectIndexed;
exports.name = 'STA';
