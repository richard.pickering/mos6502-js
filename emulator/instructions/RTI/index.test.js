
const rti = require("./index");
const emulatorModule = require("../../emulator");

let emulator = {};

beforeEach(() => {
    emulator = new emulatorModule.Emulator();
});

test("Name is RTI", () => {
    expect(rti.name).toBe('RTI');
});

test("Pulls the processor status register from the stack followed by the program counter", () => {

    emulator.pushToStack(0x17);
    emulator.pushToStack(0x21);
    emulator.pushToStack(0b10110110);

    const result = rti.operation(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(1);

    expect(emulator.registers.programCounter).toBe(0x1721);
    expect(emulator.processorStatus.value).toBe(0b10110110);
});
