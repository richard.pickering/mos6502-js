/*
 * Pulls the processor status flags followed by the program counter from the stack.
 */

const result = {
    cycles: 6,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.processorStatus.value = emulator.pullFromStack();
    emulator.registers.programCounterLow = emulator.pullFromStack();
    emulator.registers.programCounterHigh = emulator.pullFromStack();

    return result;
};

exports.name = 'RTI';
