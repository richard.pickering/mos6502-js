
const bne = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            programCounter: 0
        },
        processorStatus: {
            zero: false
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is BNE", () => {
    expect(bne.name).toBe('BNE');
});

test("Branches if zero flag is clear", () => {

    emulator.processorStatus.zero = false;
    emulator.memory.setByte(1, 10);

    const result = bne.operation(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);
    expect(emulator.registers.programCounter).toBe(12);
});

test("Takes more cycles if branches to new memory page", () => {

    emulator.processorStatus.zero = false;
    emulator.registers.programCounter = 0xFD;
    emulator.memory.setByte(0xFE, 10);

    const result = bne.operation(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);
    expect(emulator.registers.programCounter).toBe(0x109);
});

test("Does not branch if the zero flag is set", () => {

    emulator.processorStatus.zero = true;
    emulator.registers.programCounter = 0;
    emulator.memory.setByte(1, 10);

    const result = bne.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});
