/*
 * Clears the carry processor status flag.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.processorStatus.carry = false;

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'CLC';
