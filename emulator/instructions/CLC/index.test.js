
const clc = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                carry: false
            }
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is CLC", () => {
    expect(clc.name).toBe('CLC');
});

test("Operation succeeds", () => {

    emulator.registers.processorStatus.carry = true;

    const result = clc.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.processorStatus.carry).toBeFalsy();

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
