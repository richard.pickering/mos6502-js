/*
 * Compares the X register value to a memory held value (X - M) setting the following flags:
 * carry if X >= M
 * zero if X == M
 * negative if X - M has bit 7 set
 */

const addressing = require("../../utils/addressing");

const immediateResult = {
    cycles: 2,
    payloadLength: 2
};

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

/**
 * Performs the core of the operation, setting processor status flags appropriately.
 * @param emulator
 * @param {number} operand
 */
function operation(emulator, operand) {

    const x = emulator.registers.x;

    const result = x - operand;

    emulator.processorStatus.determineNegativeAndZero(result);
    emulator.processorStatus.carry = x >= operand;
}

function immediate(emulator) {

    const value = addressing.getImmediateValue(emulator);

    operation(emulator, value);

    emulator.advanceExecutionPointer(2);

    return immediateResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}


function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

exports.operation = operation;
exports.immediate = immediate;
exports.zeroPage = zeroPage;
exports.absolute = absolute;
exports.name = 'CPX';
