
const cpx = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        registers: {
            programCounter: 0,
            x: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is CPX", () => {
    expect(cpx.name).toBe("CPX");
});

/* ===== Operation ===== */

test("Equality sets carry to true, zero and negative are determined against a value of 0", () => {

    emulator.registers.x = 10;
    cpx.operation(emulator, 10);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
});

test("X > M sets carry to true, zero and negative determined correctly", () => {

    emulator.registers.x = 30;
    cpx.operation(emulator, 20);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
});

test("X < M sets carry to false, zero and negative determined accordingly", () => {

    emulator.registers.x = 20;
    cpx.operation(emulator, 30);

    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(-10);
});

/* ===== Addressing modes ===== */

test("Immediate mode addressing succeeds", () => {

    emulator.registers.x = 10;

    emulator.memory.setByte(1, 10);

    const result = cpx.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page addressing succeeds", () => {

    emulator.registers.x = 10;

    emulator.memory.setByte(1, 0x40);
    emulator.memory.setByte(0x40, 10);

    const result = cpx.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.x = 10;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 10);

    const result = cpx.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
