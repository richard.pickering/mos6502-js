/**
 * Transfers the contents of the X register to the A register, setting processor status flags as appropriate.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    const xValue = emulator.registers.x;

    emulator.registers.accumulator = xValue;

    emulator.processorStatus.determineNegativeAndZero(xValue);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'TXA';
