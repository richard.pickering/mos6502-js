
const txa = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            x: 0,
            accumulator: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is TXA", () => {
    expect(txa.name).toBe("TXA");
});

test("Transfers the contents of the X register to the accumulator", () => {

    emulator.registers.x = 10;

    const result = txa.operation(emulator);

    expect(emulator.registers.accumulator).toBe(10);
    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
