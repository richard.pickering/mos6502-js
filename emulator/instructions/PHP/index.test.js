
const php = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                carry: false
            }
        },
        pushToStack: jest.fn(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is PHP", () => {
    expect(php.name).toBe('PHP');
});

test("Operation succeeds", () => {

    emulator.registers.processorStatus.value = 0b11111111;

    const result = php.operation(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(1);
    expect(emulator.pushToStack).toBeCalledWith(0b11111111);

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
