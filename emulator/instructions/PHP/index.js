/*
 * Pushes the processor status value to the stack.
 */

const result = {
    cycles: 3,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.pushToStack(emulator.registers.processorStatus.value);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'PHP';
