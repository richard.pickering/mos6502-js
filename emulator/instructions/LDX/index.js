/*
 * Loads a value into the X register from memory or directly from the operation itself.
 */

const addressing = require("../../utils/addressing");

const immediateResult = {
    cycles: 2,
    payloadLength: 2
};

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageYResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

function immediate(emulator) {

    const value = addressing.getImmediateValue(emulator);
    emulator.registers.x = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return immediateResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.x = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageY(emulator) {

    const address = addressing.getZeroPageYAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.x = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return zeroPageYResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.x = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteY(emulator) {

    const addressingResult = addressing.getAbsoluteYAddress(emulator);
    const value = emulator.memory.getByte(addressingResult.address);
    emulator.registers.x = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteResultIfPageCrossed : absoluteResult;
}

exports.immediate = immediate;
exports.zeroPage = zeroPage;
exports.zeroPageY = zeroPageY;
exports.absolute = absolute;
exports.absoluteY = absoluteY;
exports.name = 'LDX';
