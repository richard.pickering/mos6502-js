/**
 * Transfers the contents of the stack pointer to the X register, setting processor status flags as appropriate.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    const stackValue = emulator.registers.stackPointer;

    emulator.registers.x = stackValue;

    emulator.processorStatus.determineNegativeAndZero(stackValue);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'TSX';
