
const tsx = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            x: 0,
            stackPointer: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is TSX", () => {
    expect(tsx.name).toBe('TSX');
});

test("Transfers the contents of the stack pointer to the X register", () => {

    emulator.registers.stackPointer = 10;

    const result = tsx.operation(emulator);

    expect(emulator.registers.x).toBe(10);
    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
