/*
 * Clears the overflow processor status flag.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.processorStatus.overflow = false;

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'CLV';
