
const clv = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                overflow: false
            }
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is CLV", () => {
    expect(clv.name).toBe('CLV');
});

test("Operation succeeds", () => {

    emulator.registers.processorStatus.overflow = true;

    const result = clv.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.processorStatus.overflow).toBeFalsy();

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
