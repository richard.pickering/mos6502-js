/*
 * Add with carry performs an addition between the accumulator and a memory location. If overflow occurs, the carry bit is set.
 * This enables multi-byte addition to be performed.
 */

const addressing = require("../../utils/addressing");
const binaryUtils = require("../../utils/binaryUtils");

const accumulatorResult = {
    cycles: 2,
    payloadLength: 1
};

const zeroPageResult = {
    cycles: 5,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 6,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 6,
    payloadLength: 3
};

const absoluteXResult = {
    cycles: 7,
    payloadLength: 3
};

function operation(emulator, operand) {

    emulator.processorStatus.carry = binaryUtils.isBitSet(7, operand);

    let result = operand << 1;

    result = binaryUtils.ensureXBits(result, 8).clampedResult;

    emulator.processorStatus.determineNegativeAndZero(result);

    return result;
}

function accumulator(emulator) {

    emulator.registers.accumulator = operation(emulator, emulator.registers.accumulator);

    emulator.advanceExecutionPointer();

    return accumulatorResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);

    const result = operation(emulator, emulator.memory.getByte(address));

    emulator.memory.setByte(address, result);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const address = addressing.getZeroPageXAddress(emulator);

    const result = operation(emulator, emulator.memory.getByte(address));

    emulator.memory.setByte(address, result);

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    const result = operation(emulator, emulator.memory.getByte(address));

    emulator.memory.setByte(address, result);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const address = addressing.getAbsoluteXAddress(emulator).address;

    const result = operation(emulator, emulator.memory.getByte(address));

    emulator.memory.setByte(address, result);

    emulator.advanceExecutionPointer(3);

    return absoluteXResult;
}

exports.accumulator = accumulator;
exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.name = 'ASL';

exports.operation = operation;
