
const asl = require("./index");

const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        advanceExecutionPointer: jest.fn(),
        processorStatus: {
            break: false,
            value: 0,
            carry: false,
            determineNegativeAndZero: jest.fn()
        },
        memory: new memory.Memory(),
        registers: {
            programCounter: 0,
            programCounterHigh: 0,
            programCounterLow: 0x10,
            accumulator: 0
        },
        pushToStack: jest.fn()
    };
});

test("Name is ASL", () => {
    expect(asl.name).toBe('ASL');
});

test("Operation succeeds and left shifts the operand", () => {

    expect(asl.operation(emulator, 0b01011111)).toBe(0b10111110);

    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b10111110);
});

test("Operation causes carry to be set to the old value of b7", () => {
    expect(asl.operation(emulator, 0b10000000)).toBe(0b0);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b0);
});

test("Accumulator succeeds", () => {

    emulator.registers.accumulator = 0b01011111;

    const result = asl.accumulator(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);

    expect(emulator.registers.accumulator).toBe(0b10111110);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.advanceExecutionPointer).toBeCalled();
});

test("Zero page addressing succeeds", () => {

    emulator.memory.setByte(1, 3);
    emulator.memory.setByte(3, 0b01011111);

    const result = asl.zeroPage(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(3)).toBe(0b10111110);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.x = 1;
    emulator.memory.setByte(1, 2);
    emulator.memory.setByte(3, 0b01011111);

    const result = asl.zeroPageX(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(3)).toBe(0b10111110);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 0b01011111);

    const result = asl.absolute(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(3);

    expect(emulator.memory.getByte(0x201)).toBe(0b10111110);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.x = 0x02;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x203, 0b01011111);

    const result = asl.absoluteX(emulator);

    expect(result.cycles).toBe(7);
    expect(result.payloadLength).toBe(3);

    expect(emulator.memory.getByte(0x203)).toBe(0b10111110);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
