
const dec = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            programCounter: 0,
            x: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is DEC", () => {
    expect(dec.name).toBe('DEC');
});

test("Zero page addressing succeeds", () => {

    emulator.memory.setByte(1, 0x40);
    emulator.memory.setByte(0x40, 10);

    const result = dec.zeroPage(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(0x40)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.x = 2;
    emulator.memory.setByte(1, 0x3E);
    emulator.memory.setByte(0x40, 10);

    const result = dec.zeroPageX(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(0x40)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 10);

    const result = dec.absolute(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x201)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.x = 3;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x204, 10);

    const result = dec.absoluteX(emulator);

    expect(result.cycles).toBe(7);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x204)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
