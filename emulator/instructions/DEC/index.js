/*
 * Decrements the value at the specified memory location, setting zero and negative flags as needed.
 */

const addressing = require("../../utils/addressing");

const zeroPageResult = {
    cycles: 5,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 6,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 6,
    payloadLength: 3
};

const absoluteXResult = {
    cycles: 7,
    payloadLength: 3
};

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);

    const result = emulator.memory.getByte(address) + 1;

    emulator.memory.setByte(address, result);

    emulator.processorStatus.determineNegativeAndZero(result);
    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const address = addressing.getZeroPageXAddress(emulator);

    const result = emulator.memory.getByte(address) + 1;

    emulator.memory.setByte(address, result);

    emulator.processorStatus.determineNegativeAndZero(result);
    emulator.advanceExecutionPointer(2);
    return zeroPageXResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    const result = emulator.memory.getByte(address) + 1;

    emulator.memory.setByte(address, result);

    emulator.processorStatus.determineNegativeAndZero(result);
    emulator.advanceExecutionPointer(3);
    return absoluteResult;
}

function absoluteX(emulator) {

    const address = addressing.getAbsoluteXAddress(emulator).address;

    const result = emulator.memory.getByte(address) + 1;

    emulator.memory.setByte(address, result);

    emulator.processorStatus.determineNegativeAndZero(result);

    emulator.advanceExecutionPointer(3);
    return absoluteXResult;
}

exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.name = 'DEC';
