/*
 * Logical AND operation performed bit by bit on the accumulator using a byte of memory as the operand.
 */

const addressing = require("../../utils/addressing");

const immediateResult = {
    cycles: 2,
    payloadLength: 2
};

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteXResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteXResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

const absoluteYResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteYResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

const indexedIndirectResult = {
    cycles: 6,
    payloadLength: 2
};

const indirectIndexedResult = {
    cycles: 5,
    payloadLength: 2
};

const indirectIndexedResultIfPageCrossed = {
    cycles: 6,
    payloadLength: 2
};

function operation(emulator, operand) {

    const accumulatorValue = emulator.registers.accumulator;
    const result = accumulatorValue & operand;

    emulator.registers.accumulator = result;
    emulator.processorStatus.determineNegativeAndZero(result);
}

function immediate(emulator) {

    operation(emulator, addressing.getImmediateValue(emulator));

    emulator.advanceExecutionPointer(2);

    return immediateResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const address = addressing.getZeroPageXAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const addressingResult = addressing.getAbsoluteXAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteXResultIfPageCrossed : absoluteXResult;
}

function absoluteY(emulator) {

    const addressingResult = addressing.getAbsoluteYAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteYResultIfPageCrossed : absoluteYResult;
}

function indexedIndirect(emulator) {

    const address = addressing.getIndexedIndirectAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return indexedIndirectResult;
}

function indirectIndexed(emulator) {

    const addressingResult = addressing.getIndirectIndexedAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(2);

    return addressingResult.pageCrossed ? indirectIndexedResultIfPageCrossed : indirectIndexedResult;
}

exports.immediate = immediate;
exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.absoluteY = absoluteY;
exports.indexedIndirect = indexedIndirect;
exports.indirectIndexed = indirectIndexed;
exports.name = 'AND';

exports.operation = operation;
