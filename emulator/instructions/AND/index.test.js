
const and = require("./index");

const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        advanceExecutionPointer: jest.fn(),
        processorStatus: {
            break: false,
            value: 0,
            determineNegativeAndZero: jest.fn()
        },
        memory: new memory.Memory(),
        registers: {
            programCounter: 0,
            programCounterHigh: 0,
            programCounterLow: 0x10,
            accumulator: 0
        },
        pushToStack: jest.fn()
    };
});

test("Name is AND", () => {
    expect(and.name).toBe('AND');
});

test("Operation succeeds and performs a logical AND on two values", () => {

    emulator.registers.accumulator = 0b01011010;

    and.operation(emulator, 0b01010001);

    expect(emulator.registers.accumulator).toBe(0b01010000);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b01010000);
});

test("Immediate mode addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.memory.setByte(1, 0b01010001);

    const result = and.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Zero page addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.memory.setByte(1, 3);
    emulator.memory.setByte(3, 0b01010001);

    const result = and.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.x = 1;
    emulator.memory.setByte(1, 2);
    emulator.memory.setByte(3, 0b01010001);

    const result = and.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 0b01010001);

    const result = and.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.x = 0x02;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x203, 0b01010001);

    const result = and.absoluteX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Absolute X addressing takes more cycles if memory pages are crossed", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.x = 0x02;

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x0);
    emulator.memory.setByte(0x101, 0b01010001);

    const result = and.absoluteX(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Absolute Y addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.y = 0x02;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x203, 0b01010001);

    const result = and.absoluteY(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Absolute Y addressing takes more cycles if memory pages are crossed", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.y = 0x02;

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x0);
    emulator.memory.setByte(0x101, 0b01010001);

    const result = and.absoluteY(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Indexed indirect addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.x = 2;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(12, 0x01);
    emulator.memory.setByte(13, 0x02);
    emulator.memory.setByte(0x201, 0b01010001);

    const result = and.indexedIndirect(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Indirect indexed addressing succeeds", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.y = 2;

    emulator.memory.setByte(1, 12);
    emulator.memory.setByte(12, 0x01);
    emulator.memory.setByte(13, 0x02);
    emulator.memory.setByte(0x203, 0b01010001);

    const result = and.indirectIndexed(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});

test("Indirect indexed takes more cycles if memory page is crossed", () => {

    emulator.registers.accumulator = 0b01011010;
    emulator.registers.y = 2;

    emulator.memory.setByte(1, 12);
    emulator.memory.setByte(12, 0xFF);
    emulator.memory.setByte(13, 0x0);
    emulator.memory.setByte(0x101, 0b01010001);

    const result = and.indirectIndexed(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b01010000);
});
