
const pla = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            accumulator: 0
        },
        pullFromStack: jest.fn(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is PLA", () => {
    expect(pla.name).toBe('PLA');
});

test("Operation succeeds", () => {

    emulator.pullFromStack.mockReturnValue(1);

    const result = pla.operation(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(1);
    expect(emulator.pullFromStack).toBeCalled();
    expect(emulator.registers.accumulator).toBe(1);

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
