/*
 * Performs a branch if the zero flag is set.
 */

const branching = require("../../utils/branching");

const resultIfNoBranch = {
    cycles: 2,
    payloadLength: 2
};

const resultIfBranches = {
    cycles: 3,
    payloadLength: 2
};

const resultIfBranchToNewPage = {
    cycles: 4,
    payloadLength: 2
};

exports.operation = function(emulator) {

    if (emulator.processorStatus.zero) {

        const branchResult = branching.branch(emulator);

        return branchResult.pageCrossed ? resultIfBranchToNewPage : resultIfBranches;
    } else {
        emulator.advanceExecutionPointer(2);
    }

    return resultIfNoBranch;
};

exports.name = 'BEQ';
