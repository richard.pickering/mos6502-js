
const cpy = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        registers: {
            programCounter: 0,
            y: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is CPY", () => {
    expect(cpy.name).toBe('CPY');
});

/* ===== Operation ===== */

test("Equality sets carry to true, zero and negative are determined against a value of 0", () => {

    emulator.registers.y = 10;
    cpy.operation(emulator, 10);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
});

test("Y > M sets carry to true, zero and negative determined correctly", () => {

    emulator.registers.y = 30;
    cpy.operation(emulator, 20);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
});

test("Y < M sets carry to false, zero and negative determined accordingly", () => {

    emulator.registers.y = 20;
    cpy.operation(emulator, 30);

    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(-10);
});

/* ===== Addressing modes ===== */

test("Immediate mode addressing succeeds", () => {

    emulator.registers.y = 10;

    emulator.memory.setByte(1, 10);

    const result = cpy.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page addressing succeeds", () => {

    emulator.registers.y = 10;

    emulator.memory.setByte(1, 0x40);
    emulator.memory.setByte(0x40, 10);

    const result = cpy.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.y = 10;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 10);

    const result = cpy.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
