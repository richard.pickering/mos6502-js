
const nop = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is NOP", () => {
    expect(nop.name).toBe('NOP');
});

test("Operation succeeds", () => {

    const result = nop.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
