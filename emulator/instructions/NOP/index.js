/*
 * No-operation
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'NOP';
