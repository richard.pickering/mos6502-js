
const pha = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                carry: false
            }
        },
        pushToStack: jest.fn(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is PHA", () => {
    expect(pha.name).toBe('PHA');
});

test("Operation succeeds", () => {

    emulator.registers.accumulator = 1;

    const result = pha.operation(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(1);
    expect(emulator.pushToStack).toBeCalledWith(1);

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
