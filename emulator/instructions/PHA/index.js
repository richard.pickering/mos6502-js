/*
 * Pushes the accumulator register's value to the stack.
 */

const result = {
    cycles: 3,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.pushToStack(emulator.registers.accumulator);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'PHA';
