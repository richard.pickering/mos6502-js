
const jsr = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            programCounter: 0,
            programCounterHigh: 0,
            programCounterLow: 1
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn(),
        pushToStack: jest.fn()
    };
});

test("Name is JSR", () => {
    expect(jsr.name).toBe('JSR');
});

test("Operation succeeds", () => {

    emulator.memory.setByte(1, 0x20);
    emulator.memory.setByte(2, 0x01);

    const result = jsr.operation(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(3);
    expect(emulator.registers.programCounter).toBe(0x0120);
    expect(emulator.pushToStack).toHaveBeenNthCalledWith(1, 0);
    expect(emulator.pushToStack).toHaveBeenNthCalledWith(2, 1);
});
