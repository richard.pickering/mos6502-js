/*
 * Sets the program counter to the address specified by the operand such that the program counter 'jumps' to that
 * instruction. Also adds the return address (the location of the last byte of this instruction) to the stack.
 */

const addressing = require("../../utils/addressing");

const absoluteResult = {
    cycles: 6,
    payloadLength: 3
};

exports.operation = function(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    emulator.registers.programCounter += 3;

    emulator.pushToStack(emulator.registers.programCounterHigh);
    emulator.pushToStack(emulator.registers.programCounterLow);

    emulator.registers.programCounter = address;

    return absoluteResult;
};

exports.name = 'JSR';
