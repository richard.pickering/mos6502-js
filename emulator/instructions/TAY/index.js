/**
 * Transfers the contents of the accumulator to the Y register, setting processor status flags as appropriate.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    const accumulatorValue = emulator.registers.accumulator;

    emulator.registers.y = accumulatorValue;

    emulator.processorStatus.determineNegativeAndZero(accumulatorValue);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'TAY';
