
const tay = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            y: 0,
            accumulator: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is TAY", () => {
    expect(tay.name).toBe('TAY');
});

test("Transfers the contents of the accumulator register to the Y register", () => {

    emulator.registers.accumulator = 10;

    const result = tay.operation(emulator);

    expect(emulator.registers.y).toBe(10);
    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
