/*
 * Sets the decimal mode processor status flag to 1.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.processorStatus.decimal = true;

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'SED';
