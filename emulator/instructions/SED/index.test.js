
const sed = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                decimal: false
            }
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is SED", () => {
    expect(sed.name).toBe('SED');
});

test("Operation succeeds", () => {

    const result = sed.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.processorStatus.decimal).toBeTruthy();

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
