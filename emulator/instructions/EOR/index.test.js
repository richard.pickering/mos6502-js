
const eor = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        registers: {
            programCounter: 0,
            x: 0,
            y: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is EOR", () => {
    expect(eor.name).toBe('EOR');
});

/* ===== Operation ===== */

test("Operation resulting in zero behaves as expected", () => {

    emulator.registers.accumulator = 0b11011011;
    eor.operation(emulator, 0b11011011);

    expect(emulator.registers.accumulator).toBe(0);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
});

test("Operation with non-zero result behaves as expected", () => {

    emulator.registers.accumulator = 0b11011011;
    eor.operation(emulator, 0b00100100);

    expect(emulator.registers.accumulator).toBe(0b11111111);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b11111111);
});

/* ===== Addressing modes ===== */

test("Immediate mode addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;

    emulator.memory.setByte(1, 0b11010000);

    const result = eor.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;

    emulator.memory.setByte(1, 0x40);
    emulator.memory.setByte(0x40, 0b11010000);

    const result = eor.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.x = 3;

    emulator.memory.setByte(1, 0x40);
    emulator.memory.setByte(0x43, 0b11010000);

    const result = eor.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 0b11010000);

    const result = eor.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.x = 1;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x202, 0b11010000);

    const result = eor.absoluteX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing takes more cycles if pages are crossed", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.x = 0xFF;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x300, 0b11010000);

    const result = eor.absoluteX(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute Y addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.y = 1;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x202, 0b11010000);

    const result = eor.absoluteY(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute Y addressing takes more cycles if pages are crossed", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.y = 0xFF;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x300, 0b11010000);

    const result = eor.absoluteY(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Indexed indirect addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.x = 3;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(13, 0x01);
    emulator.memory.setByte(14, 0x02);
    emulator.memory.setByte(0x201, 0b11010000);

    const result = eor.indexedIndirect(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Indirect indexed addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.y = 3;

    emulator.memory.setByte(1, 13);
    emulator.memory.setByte(13, 0x01);
    emulator.memory.setByte(14, 0x02);
    emulator.memory.setByte(0x204, 0b11010000);

    const result = eor.indirectIndexed(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Indirect indexed addressing takes more cycles if pages are crossed", () => {

    emulator.registers.accumulator = 0b11011011;
    emulator.registers.y = 0xFF;

    emulator.memory.setByte(1, 13);
    emulator.memory.setByte(13, 0x01);
    emulator.memory.setByte(14, 0x02);
    emulator.memory.setByte(0x300, 0b11010000);

    const result = eor.indirectIndexed(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0b00001011);
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00001011);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});
