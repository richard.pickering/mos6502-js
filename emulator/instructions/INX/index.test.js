
const inx = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            x: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is INX", () => {
    expect(inx.name).toBe('INX');
});

test("Operation succeeds", () => {

    emulator.registers.x = 10;

    const result = inx.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.x).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
