
const jmp = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            programCounter: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is JMP", () => {
    expect(jmp.name).toBe('JMP');
});

test("Absolute addressing succeeds", () => {

    emulator.memory.setByte(1, 0x20);
    emulator.memory.setByte(2, 0x01);

    const result = jmp.absolute(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(3);
    expect(emulator.registers.programCounter).toBe(0x0120);
});

test("Indirect addressing succeeds", () => {

    emulator.memory.setByte(1, 0x20);
    emulator.memory.setByte(2, 0x01);

    emulator.memory.setByte(0x0120, 0xFC);
    emulator.memory.setByte(0x0121, 0xBA);

    const result = jmp.indirect(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);
    expect(emulator.registers.programCounter).toBe(0xBAFC);
});
