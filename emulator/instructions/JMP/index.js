/*
 * Sets the program counter to the address specified by the operand such that the program counter 'jumps' to that
 * instruction.
 */

const addressing = require("../../utils/addressing");

const absoluteResult = {
    cycles: 3,
    payloadLength: 3
};

const indirectResult = {
    cycles: 5,
    payloadLength: 3
};

function operation(emulator, address) {

    emulator.registers.programCounter = address;
}

function absolute(emulator) {

    operation(emulator, addressing.getAbsoluteAddress(emulator));

    return absoluteResult;
}

function indirect(emulator) {

    operation(emulator, addressing.getIndirectAddress(emulator));

    return indirectResult;
}

exports.absolute = absolute;
exports.indirect = indirect;
exports.name = 'JMP';
