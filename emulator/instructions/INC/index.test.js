
const inc = require("./index");
const memoryModule = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            x: 0,
            programCounter: 0
        },
        memory: new memoryModule.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is INC", () => {
    expect(inc.name).toBe('INC');
});

test("Zero page addressing succeeds", () => {

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(10, 10);

    const result = inc.zeroPage(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(10)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.memory.setByte(1, 7);
    emulator.memory.setByte(10, 10);

    emulator.registers.x = 3;

    const result = inc.zeroPageX(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(10)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x201, 10);

    const result = inc.absolute(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x201)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.registers.x = 0x10;

    emulator.memory.setByte(0x211, 10);

    const result = inc.absoluteX(emulator);

    expect(result.cycles).toBe(7);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x211)).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
