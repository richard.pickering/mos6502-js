/*
 * Stores the value of the Y register at a specified address in memory.
 */

const addressing = require("../../utils/addressing");

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

function zeroPage(emulator) {

    const zeroPageAddress = addressing.getZeroPageAddress(emulator);
    const yValue = emulator.registers.y;

    emulator.memory.setByte(zeroPageAddress, yValue);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const zeroPageAddress = addressing.getZeroPageXAddress(emulator);
    const yValue = emulator.registers.y;

    emulator.memory.setByte(zeroPageAddress, yValue);

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const absoluteAddress = addressing.getAbsoluteAddress(emulator);
    const yValue = emulator.registers.y;

    emulator.memory.setByte(absoluteAddress, yValue);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.name = 'STY';
