
const sty = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            y: 0,
            x: 0,
            programCounter: 0
        },
        advanceExecutionPointer: jest.fn(),
        memory: new memory.Memory()
    };
});

test("Name is STY", () => {
    expect(sty.name).toBe('STY');
});

test("Zero page addressing succeeds", () => {

    emulator.memory.setByte(1, 128);
    emulator.registers.y = 192;

    const result = sty.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(128)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.memory.setByte(1, 123);
    emulator.registers.x = 5;
    emulator.registers.y = 192;

    const result = sty.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(128)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.memory.setByte(1, 0x4);
    emulator.memory.setByte(2, 0x2);

    emulator.registers.y = 192;

    const result = sty.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.memory.getByte(0x204)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

