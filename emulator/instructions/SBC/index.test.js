
const sbc = require("./index");

const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        advanceExecutionPointer: jest.fn(),
        processorStatus: {
            break: false,
            value: 0,
            carry: false,
            determineNegativeAndZero: jest.fn()
        },
        memory: new memory.Memory(),
        registers: {
            programCounter: 0,
            programCounterHigh: 0,
            programCounterLow: 0x10,
            accumulator: 0
        },
        pushToStack: jest.fn()
    };
});

test("Operation succeeds and subtracts two values", () => {

    emulator.processorStatus.carry = true;

    emulator.registers.accumulator = 0x06;

    sbc.operation(emulator, 0x04);

    expect(emulator.registers.accumulator).toBe(0x02);
    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0x02);
});

test("Clears carry if subtraction overflows", () => {

    emulator.processorStatus.carry = true;

    emulator.registers.accumulator = 0x02;

    sbc.operation(emulator, 0x04);

    expect(emulator.registers.accumulator).toBe(0xFE);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0xFE);
});

test("If the carry bit is clear, the not of the carry bit is taken into account within the calculation", () => {

    emulator.processorStatus.carry = false;
    emulator.registers.accumulator = 0x06;

    sbc.operation(emulator, 0x04);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0x01);
});

test("Immediate mode addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;
    emulator.memory.setByte(1, 0x04);

    const result = sbc.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Zero page addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;
    emulator.memory.setByte(1, 3);
    emulator.memory.setByte(3, 0x04);

    const result = sbc.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.x = 1;
    emulator.memory.setByte(1, 2);
    emulator.memory.setByte(3, 0x04);

    const result = sbc.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Absolute addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 0x04);

    const result = sbc.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.x = 0x02;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x203, 0x04);

    const result = sbc.absoluteX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Absolute X addressing takes more cycles if memory pages are crossed", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.x = 0x02;

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x0);
    emulator.memory.setByte(0x101, 0x04);

    const result = sbc.absoluteX(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Absolute Y addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.y = 0x02;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x203, 0x04);

    const result = sbc.absoluteY(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Absolute Y addressing takes more cycles if memory pages are crossed", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.y = 0x02;

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x0);
    emulator.memory.setByte(0x101, 0x04);

    const result = sbc.absoluteY(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Indexed indirect addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.x = 2;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(12, 0x01);
    emulator.memory.setByte(13, 0x02);
    emulator.memory.setByte(0x201, 0x04);

    const result = sbc.indexedIndirect(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Indirect indexed addressing succeeds", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.y = 2;

    emulator.memory.setByte(1, 12);
    emulator.memory.setByte(12, 0x01);
    emulator.memory.setByte(13, 0x02);
    emulator.memory.setByte(0x203, 0x04);

    const result = sbc.indirectIndexed(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});

test("Indirect indexed takes more cycles if memory page is crossed", () => {

    emulator.registers.accumulator = 0x06;
    emulator.registers.y = 2;

    emulator.memory.setByte(1, 12);
    emulator.memory.setByte(12, 0xFF);
    emulator.memory.setByte(13, 0x0);
    emulator.memory.setByte(0x101, 0x04);

    const result = sbc.indirectIndexed(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(0x01);
    expect(emulator.processorStatus.carry).toBeTruthy();
});
