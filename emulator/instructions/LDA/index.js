/*
 * Loads a value into the accumulator register from memory or directly from the operation itself.
 */

const addressing = require("../../utils/addressing");

const immediateResult = {
    cycles: 2,
    payloadLength: 2
};

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

const indexedIndirectResult = {
    cycles: 6,
    payloadLength: 2
};

const indirectIndexedResult = {
    cycles: 5,
    payloadLength: 2
};

const indirectIndexedResultIfPageCrossed = {
    cycles: 6,
    payloadLength: 2
};

function immediate(emulator) {

    const value = addressing.getImmediateValue(emulator);
    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return immediateResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const address = addressing.getZeroPageXAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const addressingResult = addressing.getAbsoluteXAddress(emulator);
    const value = emulator.memory.getByte(addressingResult.address);
    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteResultIfPageCrossed : absoluteResult;
}

function absoluteY(emulator) {

    const addressingResult = addressing.getAbsoluteYAddress(emulator);
    const value = emulator.memory.getByte(addressingResult.address);
    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteResultIfPageCrossed : absoluteResult;
}

function indexedIndirect(emulator) {

    const address = addressing.getIndexedIndirectAddress(emulator);
    const value = emulator.memory.getByte(address);

    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return indexedIndirectResult;
}

function indirectIndexed(emulator) {

    const addressingResult = addressing.getIndirectIndexedAddress(emulator);
    const value = emulator.memory.getByte(addressingResult.address);

    emulator.registers.accumulator = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return addressingResult.pageCrossed ? indirectIndexedResultIfPageCrossed : indirectIndexedResult;
}

exports.immediate = immediate;
exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.absoluteY = absoluteY;
exports.indexedIndirect = indexedIndirect;
exports.indirectIndexed = indirectIndexed;
exports.name = 'LDA';
