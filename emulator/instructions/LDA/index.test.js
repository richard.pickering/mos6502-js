
const lda = require("./index");

const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            accumulator: 0,
            programCounter: 0,
            x: 0,
            y: 0
        },
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        memory: new memory.Memory(),
        pushToStack: jest.fn(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is LDA", () => {
    expect(lda.name).toBe('LDA');
});

test("Immediate addressing succeeds", () => {

    emulator.registers.accumulator = 1;

    emulator.memory.setByte(1, 129);

    const result = lda.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page addressing succeeds", () => {

    emulator.registers.accumulator = 1;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(10, 129);

    const result = lda.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.x = 3;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(13, 129);

    const result = lda.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.accumulator = 1;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 129);

    const result = lda.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.x = 0x01;

    emulator.memory.setByte(1, 0x0);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 129);

    const result = lda.absoluteX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing takes more cycles if a page is crossed", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.x = 0x01;

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x0);
    emulator.memory.setByte(0x100, 129);

    const result = lda.absoluteX(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute Y addressing succeeds", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.y = 0x01;

    emulator.memory.setByte(1, 0x0);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 129);

    const result = lda.absoluteY(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute Y addressing takes more cycles if a page is crossed", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.y = 0x01;

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x0);
    emulator.memory.setByte(0x100, 129);

    const result = lda.absoluteY(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Indexed indirect addressing succeeds", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.x = 1;

    emulator.memory.setByte(1, 9);

    emulator.memory.setByte(10, 0x01);
    emulator.memory.setByte(11, 0x02);

    emulator.memory.setByte(0x201, 129);

    const result = lda.indexedIndirect(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Indirect indexed addressing succeeds", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.y = 1;

    emulator.memory.setByte(1, 10);

    emulator.memory.setByte(10, 0x0);
    emulator.memory.setByte(11, 0x02);

    emulator.memory.setByte(0x201, 129);

    const result = lda.indirectIndexed(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Indirect indexed addressing takes more cycles if a memory page is crossed", () => {

    emulator.registers.accumulator = 1;
    emulator.registers.y = 1;

    emulator.memory.setByte(1, 10);

    emulator.memory.setByte(10, 0xFF);
    emulator.memory.setByte(11, 0x00);

    emulator.memory.setByte(0x100, 129);

    const result = lda.indirectIndexed(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.accumulator).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});
