
const dex = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            x: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is DEX", () => {
    expect(dex.name).toBe('DEX');
});

test("Operation succeeds", () => {

    emulator.registers.x = 10;

    const result = dex.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.x).toBe(9);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(9);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
