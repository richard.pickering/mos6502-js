/*
 * Decrements the X register, setting zero and negative flags as needed.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.x--;

    emulator.processorStatus.determineNegativeAndZero(emulator.registers.x);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'DEX';
