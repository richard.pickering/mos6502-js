
const txs = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            x: 0,
            stackPointer: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is TXS", () => {
    expect(txs.name).toBe("TXS");
});

test("Transfers the contents of the X register to the stack pointer", () => {

    emulator.registers.x = 10;

    const result = txs.operation(emulator);

    expect(emulator.registers.stackPointer).toBe(10);
    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
