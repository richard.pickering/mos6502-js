
const adc = require("./ADC");
const and = require("./AND");
const asl = require("./ASL");
const bcc = require("./BCC");
const bcs = require("./BCS");
const beq = require("./BEQ");
const bit = require("./BIT");
const bmi = require("./BMI");
const bne = require("./BNE");
const bpl = require("./BPL");
const brk = require("./BRK");
const bvc = require("./BVC");
const bvs = require("./BVS");
const clc = require("./CLC");
const cld = require("./CLD");
const cli = require("./CLI");
const clv = require("./CLV");
const cmp = require("./CMP");
const cpx = require("./CPX");
const cpy = require("./CPY");
const dec = require("./DEC");
const dex = require("./DEX");
const dey = require("./DEY");
const eor = require("./EOR");
const inc = require("./INC");
const inx = require("./INX");
const iny = require("./INY");
const jmp = require("./JMP");
const jsr = require("./JSR");
const lda = require("./LDA");
const ldx = require("./LDX");
const ldy = require("./LDY");
const lsr = require("./LSR");
const nop = require("./NOP");
const ora = require("./ORA");
const pha = require("./PHA");
const php = require("./PHP");
const pla = require("./PLA");
const plp = require("./PLP");
const rol = require("./ROL");
const ror = require("./ROR");
const rti = require("./RTI");
const rts = require("./RTS");
const sbc = require("./SBC");
const sec = require("./SEC");
const sed = require("./SED");
const sei = require("./SEI");
const sta = require("./STA");
const stx = require("./STX");
const sty = require("./STY");
const tax = require("./TAX");
const tay = require("./TAY");
const tsx = require("./TSX");
const txa = require("./TXA");
const txs = require("./TXS");
const tya = require("./TYA");

const operations = {
    // ADC
    0x69: remap(adc.immediate, adc),
    0x65: remap(adc.zeroPage, adc),
    0x75: remap(adc.zeroPageX, adc),
    0x6D: remap(adc.absolute, adc),
    0x7D: remap(adc.absoluteX, adc),
    0x79: remap(adc.absoluteY, adc),
    0x61: remap(adc.indexedIndirect, adc),
    0x71: remap(adc.indirectIndexed, adc),
    // AND
    0x29: remap(and.immediate, and),
    0x25: remap(and.zeroPage, and),
    0x35: remap(and.zeroPageX, and),
    0x2D: remap(and.absolute, and),
    0x3D: remap(and.absoluteX, and),
    0x39: remap(and.absoluteY, and),
    0x21: remap(and.indexedIndirect, and),
    0x31: remap(and.indirectIndexed, and),
    // ASL
    0x0A: remap(asl.accumulator, asl),
    0x06: remap(asl.zeroPage, asl),
    0x16: remap(asl.zeroPageX, asl),
    0x0E: remap(asl.absolute, asl),
    0x1E: remap(asl.absoluteX, asl),
    0x90: bcc,
    0xB0: bcs,
    0xF0: beq,
    0x24: remap(bit.zeroPage, bit),
    0x2C: remap(bit.absolute, bit),
    0x30: bmi,
    0xD0: bne,
    0x10: bpl,
    0x00: brk,
    0x50: bvc,
    0x70: bvs,
    // Flag clear operations
    0x18: clc,
    0xD8: cld,
    0x58: cli,
    0xB8: clv,
    // CMP
    0xC9: remap(cmp.immediate, cmp),
    0xC5: remap(cmp.zeroPage, cmp),
    0xD5: remap(cmp.zeroPageX, cmp),
    0xCD: remap(cmp.absolute, cmp),
    0xDD: remap(cmp.absoluteX, cmp),
    0xD9: remap(cmp.absoluteY, cmp),
    0xC1: remap(cmp.indexedIndirect, cmp),
    0xD1: remap(cmp.indirectIndexed, cmp),
    // CPX
    0xE0: remap(cpx.immediate, cpx),
    0xE4: remap(cpx.zeroPage, cpx),
    0xEC: remap(cpx.absolute, cpx),
    // CPY
    0xC0: remap(cpy.immediate, cpy),
    0xC4: remap(cpy.zeroPage, cpy),
    0xCC: remap(cpy.absolute, cpy),
    // Decrement operations
    0xC6: remap(dec.zeroPage, dec),
    0xD6: remap(dec.zeroPageX, dec),
    0xCE: remap(dec.absolute, dec),
    0xDE: remap(dec.absoluteX, dec),
    0xCA: dex,
    0x88: dey,
    // EOR
    0x49: remap(eor.immediate, eor),
    0x45: remap(eor.zeroPage, eor),
    0x55: remap(eor.zeroPageX, eor),
    0x4D: remap(eor.absolute, eor),
    0x5D: remap(eor.absoluteX, eor),
    0x59: remap(eor.absoluteY, eor),
    0x41: remap(eor.indexedIndirect, eor),
    0x51: remap(eor.indirectIndexed, eor),
    // INC
    0xE6: remap(inc.zeroPage, inc),
    0xF6: remap(inc.zeroPageX, inc),
    0xEE: remap(inc.absolute, inc),
    0xFE: remap(inc.absoluteX, inc),
    // Increment operations
    0xE8: inx,
    0xC8: iny,
    // JMP
    0x4C: remap(jmp.absolute, jmp),
    0x6C: remap(jmp.indirect, jmp),
    // JSR
    0x20: jsr,
    // LDA
    0xA9: remap(lda.immediate, lda),
    0xA5: remap(lda.zeroPage, lda),
    0xB5: remap(lda.zeroPageX, lda),
    0xAD: remap(lda.absolute, lda),
    0xBD: remap(lda.absoluteX, lda),
    0xB9: remap(lda.absoluteY, lda),
    0xA1: remap(lda.indexedIndirect, lda),
    0xB1: remap(lda.indirectIndexed, lda),
    // LDX
    0xA2: remap(ldx.immediate, ldx),
    0xA6: remap(ldx.zeroPage, ldx),
    0xB6: remap(ldx.zeroPageY, ldx),
    0xAE: remap(ldx.absolute, ldx),
    0xBE: remap(ldx.absoluteY, ldx),
    // LDY
    0xA0: remap(ldy.immediate, ldy),
    0xA4: remap(ldy.zeroPage, ldy),
    0xB4: remap(ldy.zeroPageX, ldy),
    0xAC: remap(ldy.absolute, ldy),
    0xBC: remap(ldy.absoluteX, ldy),
    // LSR
    0x4A: remap(lsr.accumulator, lsr),
    0x46: remap(lsr.zeroPage, lsr),
    0x56: remap(lsr.zeroPageX, lsr),
    0x4E: remap(lsr.absolute, lsr),
    0x5E: remap(lsr.absoluteX, lsr),
    0xEA: nop,
    // ORA
    0x09: remap(ora.immediate, ora),
    0x05: remap(ora.zeroPage, ora),
    0x15: remap(ora.zeroPageX, ora),
    0x0D: remap(ora.absolute, ora),
    0x1D: remap(ora.absoluteX, ora),
    0x19: remap(ora.absoluteY, ora),
    0x01: remap(ora.indexedIndirect, ora),
    0x11: remap(ora.indirectIndexed, ora),
    // Stack push operations
    0x48: pha,
    0x08: php,
    0x68: pla,
    0x28: plp,
    // ROL
    0x2A: remap(rol.accumulator, rol),
    0x26: remap(rol.zeroPage, rol),
    0x36: remap(rol.zeroPageX, rol),
    0x2E: remap(rol.absolute, rol),
    0x3E: remap(rol.absoluteX, rol),
    // ROR
    0x6A: remap(ror.accumulator, ror),
    0x66: remap(ror.zeroPage, ror),
    0x76: remap(ror.zeroPageX, ror),
    0x6E: remap(ror.absolute, ror),
    0x7E: remap(ror.absoluteX, ror),
    0x40: rti,
    0x60: rts,
    // SBC
    0xE9: remap(sbc.immediate, sbc),
    0xE5: remap(sbc.zeroPage, sbc),
    0xF5: remap(sbc.zeroPageX, sbc),
    0xED: remap(sbc.absolute, sbc),
    0xFD: remap(sbc.absoluteX, sbc),
    0xF9: remap(sbc.absoluteY, sbc),
    0xE1: remap(sbc.indexedIndirect, sbc),
    0xF1: remap(sbc.indirectIndexed, sbc),
    // Set processor status flag operations
    0x38: sec,
    0xF8: sed,
    0x78: sei,
    // STA
    0x85: remap(sta.zeroPage, sta),
    0x95: remap(sta.zeroPageX, sta),
    0x8D: remap(sta.absolute, sta),
    0x9D: remap(sta.absoluteX, sta),
    0x99: remap(sta.absoluteY, sta),
    0x81: remap(sta.indexedIndirect, sta),
    0x91: remap(sta.indirectIndexed, sta),
    // STX
    0x86: remap(stx.zeroPage, stx),
    0x96: remap(stx.zeroPageY, stx),
    0x8E: remap(stx.absolute, stx),
    // STY
    0x84: remap(sty.zeroPage, sty),
    0x94: remap(sty.zeroPageX, sty),
    0x8C: remap(sty.absolute, sty),
    // Transfer instructions
    0xAA: tax,
    0xA8: tay,
    0xBA: tsx,
    0x8A: txa,
    0x9A: txs,
    0x98: tya
};

function remap(operation, fullInstruction) {
    return {
        operation: operation,
        name: fullInstruction.name
    };
}

/**
 * Returns the operation for the given op-code.
 * @param {number} opCode
 * @return {*}
 */
function getOperation(opCode) {

    if (operations[opCode] === undefined) {
        throw new Error(`No operation defined for op-code ${opCode}`);
    }

    return operations[opCode];
}

exports.getOperation = getOperation;
exports.operations = operations;
