
const stx = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            y: 0,
            x: 0,
            programCounter: 0
        },
        advanceExecutionPointer: jest.fn(),
        memory: new memory.Memory()
    };
});

test("Name is STX", () => {
    expect(stx.name).toBe('STX');
});

test("Zero page addressing succeeds", () => {

    emulator.memory.setByte(1, 128);
    emulator.registers.x = 192;

    const result = stx.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(128)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page Y addressing succeeds", () => {

    emulator.memory.setByte(1, 123);
    emulator.registers.y = 5;
    emulator.registers.x = 192;

    const result = stx.zeroPageY(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.memory.getByte(128)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.memory.setByte(1, 0x4);
    emulator.memory.setByte(2, 0x2);

    emulator.registers.x = 192;

    const result = stx.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.memory.getByte(0x204)).toBe(192);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
