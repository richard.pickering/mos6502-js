/*
 * Stores the value of the X register at a specified address in memory.
 */

const addressing = require("../../utils/addressing");

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageYResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

function zeroPage(emulator) {

    const zeroPageAddress = addressing.getZeroPageAddress(emulator);
    const xValue = emulator.registers.x;

    emulator.memory.setByte(zeroPageAddress, xValue);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageY(emulator) {

    const zeroPageAddress = addressing.getZeroPageYAddress(emulator);
    const xValue = emulator.registers.x;

    emulator.memory.setByte(zeroPageAddress, xValue);

    emulator.advanceExecutionPointer(2);

    return zeroPageYResult;
}

function absolute(emulator) {

    const absoluteAddress = addressing.getAbsoluteAddress(emulator);
    const xValue = emulator.registers.x;

    emulator.memory.setByte(absoluteAddress, xValue);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

exports.zeroPage = zeroPage;
exports.zeroPageY = zeroPageY;
exports.absolute = absolute;
exports.name = 'STX';
