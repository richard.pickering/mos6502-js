
const sei = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                interruptDisable: false
            }
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is SEI", () => {
    expect(sei.name).toBe('SEI');
});

test("Operation succeeds", () => {

    const result = sei.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.processorStatus.interruptDisable).toBeTruthy();

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
