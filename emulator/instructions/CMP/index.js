/*
 * Compares the accumulator value to a memory held value (A - M) setting the following flags:
 * carry if A >= M
 * zero if A == M
 * negative if A - M has bit 7 set
 */

const addressing = require("../../utils/addressing");
const binaryUtils = require("../../utils/binaryUtils");

const immediateResult = {
    cycles: 2,
    payloadLength: 2
};

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteXResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteXResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

const absoluteYResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteYResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

const indexedIndirectResult = {
    cycles: 6,
    payloadLength: 2
};

const indirectIndexedResult = {
    cycles: 5,
    payloadLength: 2
};

const indirectIndexedResultIfPageCrossed = {
    cycles: 6,
    payloadLength: 2
};

/**
 * Performs the core of the operation, setting processor status flags appropriately.
 * @param emulator
 * @param {number} operand
 */
function operation(emulator, operand) {

    const accumulator = emulator.registers.accumulator;

    let result = accumulator - operand;

    const clampResult = binaryUtils.ensureXBits(result, 8);
    result = clampResult.clampedResult;

    emulator.processorStatus.determineNegativeAndZero(result);
    emulator.processorStatus.carry = accumulator >= operand;
}

function immediate(emulator) {

    const value = addressing.getImmediateValue(emulator);

    operation(emulator, value);

    emulator.advanceExecutionPointer(2);

    return immediateResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const address = addressing.getZeroPageXAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const addressingResult = addressing.getAbsoluteXAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteXResultIfPageCrossed : absoluteXResult;
}

function absoluteY(emulator) {

    const addressingResult = addressing.getAbsoluteYAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteYResultIfPageCrossed : absoluteYResult;
}

function indexedIndirect(emulator) {

    const address = addressing.getIndexedIndirectAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return indexedIndirectResult;
}

function indirectIndexed(emulator) {

    const addressingResult = addressing.getIndirectIndexedAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(2);

    return addressingResult.pageCrossed ? indirectIndexedResultIfPageCrossed : indirectIndexedResult;
}

exports.operation = operation;
exports.immediate = immediate;
exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.absoluteY = absoluteY;
exports.indexedIndirect = indexedIndirect;
exports.indirectIndexed = indirectIndexed;
exports.name = "CMP";
