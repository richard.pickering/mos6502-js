
const cmp = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        registers: {
            programCounter: 0,
            accumulator: 0,
            x: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is CMP", () => {
    expect(cmp.name).toBe('CMP');
});

/* ===== Operation ===== */

test("Equality sets carry to true, zero and negative are determined against a value of 0", () => {

    emulator.registers.accumulator = 10;
    cmp.operation(emulator, 10);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
});

test("A > M sets carry to true, zero and negative determined correctly", () => {

    emulator.registers.accumulator = 30;
    cmp.operation(emulator, 20);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
});

test("A < M sets carry to false, zero and negative determined accordingly", () => {

    emulator.registers.accumulator = 20;
    cmp.operation(emulator, 30);

    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0x100 - 10);
});

/* ===== Addressing modes ===== */

test("Immediate mode addressing succeeds", () => {

    emulator.registers.accumulator = 10;

    emulator.memory.setByte(1, 10);

    const result = cmp.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page addressing succeeds", () => {

    emulator.registers.accumulator = 10;

    emulator.memory.setByte(1, 0x40);
    emulator.memory.setByte(0x40, 10);

    const result = cmp.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.x = 2;
    emulator.registers.accumulator = 10;
    emulator.memory.setByte(1, 0x3E);
    emulator.memory.setByte(0x40, 10);

    const result = cmp.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.accumulator = 10;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 10);

    const result = cmp.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.accumulator = 10;
    emulator.registers.x = 3;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x204, 10);

    const result = cmp.absoluteX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing takes more cycles if pages crossed", () => {

    emulator.registers.accumulator = 10;
    emulator.registers.x = 0xFF;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x300, 10);

    const result = cmp.absoluteX(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute Y addressing succeeds", () => {

    emulator.registers.accumulator = 10;
    emulator.registers.y = 3;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x204, 10);

    const result = cmp.absoluteY(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute Y addressing takes more cycles if pages crossed", () => {

    emulator.registers.accumulator = 10;
    emulator.registers.y = 0xFF;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x300, 10);

    const result = cmp.absoluteY(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Indexed indirect addressing succeeds", () => {

    emulator.registers.accumulator = 10;
    emulator.registers.x = 3;

    emulator.memory.setByte(1, 10);

    emulator.memory.setByte(13, 0x01);
    emulator.memory.setByte(14, 0x02);

    emulator.memory.setByte(0x201, 10);

    const result = cmp.indexedIndirect(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Indirect indexed addressing succeeds", () => {

    emulator.registers.accumulator = 10;
    emulator.registers.y = 3;

    emulator.memory.setByte(1, 13);

    emulator.memory.setByte(13, 0x01);
    emulator.memory.setByte(14, 0x02);

    emulator.memory.setByte(0x204, 10);

    const result = cmp.indirectIndexed(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Indirect indexed addressing takes more cycles if pages are crossed", () => {

    emulator.registers.accumulator = 10;
    emulator.registers.y = 0xFF;

    emulator.memory.setByte(1, 13);

    emulator.memory.setByte(13, 0x01);
    emulator.memory.setByte(14, 0x02);

    emulator.memory.setByte(0x300, 10);

    const result = cmp.indirectIndexed(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);
    expect(emulator.processorStatus.carry).toBeTruthy();

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});
