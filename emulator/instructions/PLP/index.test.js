
const plp = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                carry: false
            }
        },
        pullFromStack: jest.fn(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is PLP", () => {
    expect(plp.name).toBe('PLP');
});

test("Operation succeeds", () => {

    emulator.pullFromStack.mockReturnValue(0b11111111);

    const result = plp.operation(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(1);
    expect(emulator.pullFromStack).toBeCalled();
    expect(emulator.registers.processorStatus.value).toBe(0b11111111);

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
