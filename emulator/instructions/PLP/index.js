/*
 * Pulls the processor status value from the stack.
 */

const result = {
    cycles: 4,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.processorStatus.value = emulator.pullFromStack();

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'PLP';
