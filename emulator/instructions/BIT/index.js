/*
 * Performs an AND of the values in A and M, setting the zero flag if the result of the operation is 0
 * Sets Overflow and Negative flags to b6 and b7 of the result.
 */

const addressing = require("../../utils/addressing");
const binaryUtils = require("../../utils/binaryUtils");

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

function operation(emulator, operand) {

    const accumulator = emulator.registers.accumulator;

    const result = accumulator & operand;

    emulator.processorStatus.determineNegativeAndZero(result);
    emulator.processorStatus.overflow = binaryUtils.isBitSet(6, result);
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

exports.operation = operation;
exports.zeroPage = zeroPage;
exports.absolute = absolute;
exports.name = 'BIT';
