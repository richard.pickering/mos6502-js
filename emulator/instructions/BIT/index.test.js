
const bit = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            overflow: false
        },
        registers: {
            accumulator: 0,
            programCounter: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is BIT", () => {
    expect(bit.name).toBe('BIT');
});

/* ===== Operation ===== */

test("Operation succeeds", () => {

    emulator.registers.accumulator = 0b11011011;

    bit.operation(emulator, 0b11010100);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b11010000);
    expect(emulator.processorStatus.overflow).toBeTruthy();
});

/* ===== Addressing modes ===== */

test("Zero page addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(10, 0b11010100);

    const result = bit.zeroPage(emulator, 0b11010100);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b11010000);
    expect(emulator.processorStatus.overflow).toBeTruthy();

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.accumulator = 0b11011011;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 0b11010100);

    const result = bit.absolute(emulator, 0b11010100);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b11010000);
    expect(emulator.processorStatus.overflow).toBeTruthy();

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
