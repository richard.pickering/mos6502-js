
const rol = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        registers: {
            accumulator: 0,
            programCounter: 0,
            x: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is ROL", () => {
    expect(rol.name).toBe('ROL');
});

/* ===== Operation ===== */

test("Rotate left performs a logical left shift on the provided value", () => {

    emulator.processorStatus.carry = false;

    expect(rol.operation(emulator, 0b01101010)).toBe(0b11010100);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b11010100);
});

test("Rotate left fills b0 with the current value of the carry flag", () => {

    emulator.processorStatus.carry = true;

    expect(rol.operation(emulator, 0b01101010)).toBe(0b11010101);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b11010101);
});

test("Rotate left sets the carry flag to the old value of b7 (prior to shifting)", () => {

    emulator.processorStatus.carry = false;

    expect(rol.operation(emulator, 0b11010110)).toBe(0b10101100);
    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b10101100);
});

/* ===== Addressing modes ===== */

test("Accumulator rotation succeeds", () => {

    emulator.processorStatus.carry = true;
    emulator.registers.accumulator = 0b01101010;

    const result = rol.accumulator(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.accumulator).toBe(0b11010101);

    expect(emulator.advanceExecutionPointer).toBeCalled();
});

test("Zero page value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(10, 0b01101010);

    const result = rol.zeroPage(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(10)).toBe(0b11010101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.registers.x = 3;
    emulator.memory.setByte(1, 7);
    emulator.memory.setByte(10, 0b01101010);

    const result = rol.zeroPageX(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(10)).toBe(0b11010101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute address value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x201, 0b01101010);

    const result = rol.absolute(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x201)).toBe(0b11010101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X address value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.registers.x = 0x01;
    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x202, 0b01101010);

    const result = rol.absoluteX(emulator);

    expect(result.cycles).toBe(7);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x202)).toBe(0b11010101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
