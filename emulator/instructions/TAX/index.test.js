
const tax = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            x: 0,
            accumulator: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is TAX", () => {
    expect(tax.name).toBe('TAX');
});

test("Transfers the contents of the accumulator register to the X register", () => {

    emulator.registers.accumulator = 10;

    const result = tax.operation(emulator);

    expect(emulator.registers.x).toBe(10);
    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
