
const iny = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            y: 0,
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is INY", () => {
    expect(iny.name).toBe('INY');
});

test("Operation succeeds", () => {

    emulator.registers.y = 10;

    const result = iny.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.y).toBe(11);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(11);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
