
const ror = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        registers: {
            accumulator: 0,
            programCounter: 0,
            x: 0
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is ROR", () => {
    expect(ror.name).toBe("ROR");
});

/* ===== Operation ===== */

test("Rotate right performs a logical right shift on the provided value", () => {

    emulator.processorStatus.carry = false;

    expect(ror.operation(emulator, 0b01101010)).toBe(0b00110101);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00110101);
});

test("Rotate right fills b7 with the current value of the carry flag", () => {

    emulator.processorStatus.carry = true;

    expect(ror.operation(emulator, 0b01101010)).toBe(0b10110101);
    expect(emulator.processorStatus.carry).toBeFalsy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b10110101);
});

test("Rotate right sets the carry flag to the old value of b0 (prior to shifting)", () => {

    emulator.processorStatus.carry = false;

    expect(ror.operation(emulator, 0b01101011)).toBe(0b00110101);
    expect(emulator.processorStatus.carry).toBeTruthy();
    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(0b00110101);
});

/* ===== Addressing modes ===== */

test("Accumulator rotation succeeds", () => {

    emulator.processorStatus.carry = true;
    emulator.registers.accumulator = 0b01101010;

    const result = ror.accumulator(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.accumulator).toBe(0b10110101);

    expect(emulator.advanceExecutionPointer).toBeCalled();
});

test("Zero page value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(10, 0b01101010);

    const result = ror.zeroPage(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(10)).toBe(0b10110101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.registers.x = 3;
    emulator.memory.setByte(1, 7);
    emulator.memory.setByte(10, 0b01101010);

    const result = ror.zeroPageX(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(2);
    expect(emulator.memory.getByte(10)).toBe(0b10110101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute address value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x201, 0b01101010);

    const result = ror.absolute(emulator);

    expect(result.cycles).toBe(6);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x201)).toBe(0b10110101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X address value rotation succeeds", () => {

    emulator.processorStatus.carry = true;

    emulator.registers.x = 0x01;
    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);

    emulator.memory.setByte(0x202, 0b01101010);

    const result = ror.absoluteX(emulator);

    expect(result.cycles).toBe(7);
    expect(result.payloadLength).toBe(3);
    expect(emulator.memory.getByte(0x202)).toBe(0b10110101);

    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
