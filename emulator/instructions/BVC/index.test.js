
const bvc = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            programCounter: 0
        },
        processorStatus: {
            overflow: false
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is BVC", () => {
    expect(bvc.name).toBe('BVC');
});

test("Branches if overflow flag is clear", () => {

    emulator.processorStatus.overflow = false;
    emulator.memory.setByte(1, 10);

    const result = bvc.operation(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);
    expect(emulator.registers.programCounter).toBe(12);
});

test("Takes more cycles if branches to new memory page", () => {

    emulator.processorStatus.overflow = false;
    emulator.registers.programCounter = 0xFD;
    emulator.memory.setByte(0xFE, 10);

    const result = bvc.operation(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);
    expect(emulator.registers.programCounter).toBe(0x109);
});

test("Does not branch if the overflow flag is set", () => {

    emulator.processorStatus.overflow = true;
    emulator.registers.programCounter = 0;
    emulator.memory.setByte(1, 10);

    const result = bvc.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});
