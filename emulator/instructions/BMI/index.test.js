
const bmi = require("./index");
const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            programCounter: 0
        },
        processorStatus: {
            negative: false
        },
        memory: new memory.Memory(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is BMI", () => {
    expect(bmi.name).toBe('BMI');
});

test("Branches if negative flag is set", () => {

    emulator.processorStatus.negative = true;
    emulator.memory.setByte(1, 10);

    const result = bmi.operation(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);
    expect(emulator.registers.programCounter).toBe(12);
});

test("Takes more cycles if branches to new memory page", () => {

    emulator.processorStatus.negative = true;
    emulator.registers.programCounter = 0xFD;
    emulator.memory.setByte(0xFE, 10);

    const result = bmi.operation(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);
    expect(emulator.registers.programCounter).toBe(0x109);
});

test("Does not branch if the zero flag is clear", () => {

    emulator.processorStatus.negative = false;
    emulator.registers.programCounter = 0;
    emulator.memory.setByte(1, 10);

    const result = bmi.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});
