
const cli = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                interruptDisable: false
            }
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is CLI", () => {
    expect(cli.name).toBe('CLI');
});

test("Operation succeeds", () => {

    emulator.registers.processorStatus.interruptDisable = true;

    const result = cli.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.processorStatus.interruptDisable).toBeFalsy();

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
