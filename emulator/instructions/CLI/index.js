/*
 * Clears the interrupt disable processor status flag.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.processorStatus.interruptDisable = false;

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = "CLI";
