/*
 * Decrements the Y register, setting zero and negative flags as needed.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.y--;

    emulator.processorStatus.determineNegativeAndZero(emulator.registers.y);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'DEY';
