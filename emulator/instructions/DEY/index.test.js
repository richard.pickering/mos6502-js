
const dey = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            y: 0,
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is DEY", () => {
    expect(dey.name).toBe('DEY');
});

test("Operation succeeds", () => {

    emulator.registers.y = 10;

    const result = dey.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.y).toBe(9);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(9);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
