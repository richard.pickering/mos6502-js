/**
 * Transfers the contents of the Y register to the A register, setting processor status flags as appropriate.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    const yValue = emulator.registers.y;

    emulator.registers.accumulator = yValue;

    emulator.processorStatus.determineNegativeAndZero(yValue);

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'TYA';
