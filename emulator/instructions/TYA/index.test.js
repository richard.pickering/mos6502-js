
const tya = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        processorStatus: {
            determineNegativeAndZero: jest.fn()
        },
        registers: {
            y: 0,
            accumulator: 0
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is TYA", () => {
    expect(tya.name).toBe('TYA');
});

test("Transfers the contents of the Y register to the accumulator", () => {

    emulator.registers.y = 10;

    const result = tya.operation(emulator);

    expect(emulator.registers.accumulator).toBe(10);
    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(10);
    expect(emulator.advanceExecutionPointer).toBeCalled();
});
