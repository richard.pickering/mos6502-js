/*
 * Add with carry performs an addition between the accumulator and a memory location. If overflow occurs, the carry bit is set.
 * This enables multi-byte addition to be performed.
 */

const addressing = require("../../utils/addressing");
const binaryUtils = require("../../utils/binaryUtils");

const immediateResult = {
    cycles: 2,
    payloadLength: 2
};

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteXResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteXResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

const absoluteYResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteYResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

const indexedIndirectResult = {
    cycles: 6,
    payloadLength: 2
};

const indirectIndexedResult = {
    cycles: 5,
    payloadLength: 2
};

const indirectIndexedResultIfPageCrossed = {
    cycles: 6,
    payloadLength: 2
};

/**
 * Determines overflow based on if the sign of both inputs is different from the sign of the result.
 * @param {number} accumulator
 * @param {number} operand
 * @param {number} result
 * @return {boolean}
 */
function calculateOverflow(accumulator, operand, result) {

    const signedAccumulator = binaryUtils.convertToSigned(accumulator);
    const signedOperand = binaryUtils.convertToSigned(operand);
    const signedResult = binaryUtils.convertToSigned(result);

    return ((signedAccumulator ^ signedResult) & (signedOperand ^ signedResult) & 0x80) !== 0;
}

function operation(emulator, operand) {

    const accumulatorValue = emulator.registers.accumulator;

    let result = accumulatorValue + operand;

    if (emulator.processorStatus.carry) {
        result++;
    }

    const clampingResult = binaryUtils.ensureXBits(result, 8);

    if (clampingResult.leftOver > 0) {
        result = clampingResult.clampedResult;
        emulator.processorStatus.carry = true;
    } else {
        emulator.processorStatus.carry = false;
    }

    emulator.processorStatus.overflow = calculateOverflow(accumulatorValue, operand, result);

    emulator.registers.accumulator = result;
    emulator.processorStatus.determineNegativeAndZero(result);
}

function immediate(emulator) {

    operation(emulator, addressing.getImmediateValue(emulator));

    emulator.advanceExecutionPointer(2);

    return immediateResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const address = addressing.getZeroPageXAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const addressingResult = addressing.getAbsoluteXAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteXResultIfPageCrossed : absoluteXResult;
}

function absoluteY(emulator) {

    const addressingResult = addressing.getAbsoluteYAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteYResultIfPageCrossed : absoluteYResult;
}

function indexedIndirect(emulator) {

    const address = addressing.getIndexedIndirectAddress(emulator);

    operation(emulator, emulator.memory.getByte(address));

    emulator.advanceExecutionPointer(2);

    return indexedIndirectResult;
}

function indirectIndexed(emulator) {

    const addressingResult = addressing.getIndirectIndexedAddress(emulator);

    operation(emulator, emulator.memory.getByte(addressingResult.address));

    emulator.advanceExecutionPointer(2);

    return addressingResult.pageCrossed ? indirectIndexedResultIfPageCrossed : indirectIndexedResult;
}

exports.operation = operation;
exports.immediate = immediate;
exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.absoluteY = absoluteY;
exports.indexedIndirect = indexedIndirect;
exports.indirectIndexed = indirectIndexed;

exports.name = 'ADC';
