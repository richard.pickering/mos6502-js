
const sec = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                carry: false
            }
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is SEC", () => {
    expect(sec.name).toBe('SEC');
});

test("Operation succeeds", () => {

    const result = sec.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.processorStatus.carry).toBeTruthy();

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
