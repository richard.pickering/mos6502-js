/*
 * Sets the carry processor status flag to 1.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.processorStatus.carry = true;

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'SEC';
