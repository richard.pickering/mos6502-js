/*
 * Clears the decimal mode processor status flag.
 */

const result = {
    cycles: 2,
    payloadLength: 1
};

exports.operation = function(emulator) {

    emulator.registers.processorStatus.decimal = false;

    emulator.advanceExecutionPointer();

    return result;
};

exports.name = 'CLD';
