
const cld = require("./index");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            processorStatus: {
                decimal: false
            }
        },
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is CLD", () => {
    expect(cld.name).toBe("CLD");
});

test("Operation succeeds", () => {

    emulator.registers.processorStatus.decimal = true;

    const result = cld.operation(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(1);
    expect(emulator.registers.processorStatus.decimal).toBeFalsy();

    expect(emulator.advanceExecutionPointer).toBeCalled();
});
