
const ldy = require("./index");

const memory = require("../../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            accumulator: 0,
            programCounter: 0,
            x: 0,
            y: 0
        },
        processorStatus: {
            determineNegativeAndZero: jest.fn(),
            carry: false
        },
        memory: new memory.Memory(),
        pushToStack: jest.fn(),
        advanceExecutionPointer: jest.fn()
    };
});

test("Name is LDY", () => {
    expect(ldy.name).toBe("LDY");
});

test("Immediate addressing succeeds", () => {

    emulator.registers.y = 1;

    emulator.memory.setByte(1, 129);

    const result = ldy.immediate(emulator);

    expect(result.cycles).toBe(2);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.y).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page addressing succeeds", () => {

    emulator.registers.y = 1;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(10, 129);

    const result = ldy.zeroPage(emulator);

    expect(result.cycles).toBe(3);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.y).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Zero page X addressing succeeds", () => {

    emulator.registers.y = 1;
    emulator.registers.x = 3;

    emulator.memory.setByte(1, 10);
    emulator.memory.setByte(13, 129);

    const result = ldy.zeroPageX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(2);

    expect(emulator.registers.y).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(2);
});

test("Absolute addressing succeeds", () => {

    emulator.registers.y = 1;

    emulator.memory.setByte(1, 0x01);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 129);

    const result = ldy.absolute(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.y).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing succeeds", () => {

    emulator.registers.y = 1;
    emulator.registers.x = 0x01;

    emulator.memory.setByte(1, 0x0);
    emulator.memory.setByte(2, 0x02);
    emulator.memory.setByte(0x201, 129);

    const result = ldy.absoluteX(emulator);

    expect(result.cycles).toBe(4);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.y).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});

test("Absolute X addressing takes more cycles if a page is crossed", () => {

    emulator.registers.y = 1;
    emulator.registers.x = 0x01;

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x0);
    emulator.memory.setByte(0x100, 129);

    const result = ldy.absoluteX(emulator);

    expect(result.cycles).toBe(5);
    expect(result.payloadLength).toBe(3);

    expect(emulator.registers.y).toBe(129);

    expect(emulator.processorStatus.determineNegativeAndZero).toBeCalledWith(129);
    expect(emulator.advanceExecutionPointer).toBeCalledWith(3);
});
