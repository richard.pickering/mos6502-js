/*
 * Loads a value into the Y register from memory or directly from the operation itself.
 */

const addressing = require("../../utils/addressing");

const immediateResult = {
    cycles: 2,
    payloadLength: 2
};

const zeroPageResult = {
    cycles: 3,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 4,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 4,
    payloadLength: 3
};

const absoluteResultIfPageCrossed = {
    cycles: 5,
    payloadLength: 3
};

function immediate(emulator) {

    const value = addressing.getImmediateValue(emulator);
    emulator.registers.y = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return immediateResult;
}

function zeroPage(emulator) {

    const address = addressing.getZeroPageAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.y = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const address = addressing.getZeroPageXAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.y = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const address = addressing.getAbsoluteAddress(emulator);
    const value = emulator.memory.getByte(address);
    emulator.registers.y = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const addressingResult = addressing.getAbsoluteXAddress(emulator);
    const value = emulator.memory.getByte(addressingResult.address);
    emulator.registers.y = value;

    emulator.processorStatus.determineNegativeAndZero(value);

    emulator.advanceExecutionPointer(3);

    return addressingResult.pageCrossed ? absoluteResultIfPageCrossed : absoluteResult;
}

exports.immediate = immediate;
exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.name = 'LDY';
