/*
 * Shifts each of the bits in A or M 1 place to the right. The bit previously in bit 0 is then stored in the carry flag
 * Bit 7 of the result is set to zero.
 */

const addressing = require("../../utils/addressing");
const binaryUtils = require("../../utils/binaryUtils");

const accumulatorResult = {
    cycles: 2,
    payloadLength: 1
};

const zeroPageResult = {
    cycles: 5,
    payloadLength: 2
};

const zeroPageXResult = {
    cycles: 6,
    payloadLength: 2
};

const absoluteResult = {
    cycles: 6,
    payloadLength: 3
};

const absoluteXResult = {
    cycles: 7,
    payloadLength: 3
};

/**
 * Performs the rotate right operation (minus the variants of addressing modes - i.e. number of cycles taken, how far
 * to advance execution pointer, etc.).
 * @param emulator
 * @param {number} value
 * @return {number}
 */
function operation(emulator, value) {

    let newValue = value >> 1;

    emulator.processorStatus.carry = binaryUtils.isBitSet(0, value);
    emulator.processorStatus.determineNegativeAndZero(newValue);

    return newValue;
}

function accumulator(emulator) {

    const accumulatorValue = emulator.registers.accumulator;

    emulator.registers.accumulator = operation(emulator, accumulatorValue);

    emulator.advanceExecutionPointer();

    return accumulatorResult;
}

function zeroPage(emulator) {

    const zeroPageAddress = addressing.getZeroPageAddress(emulator);
    const newValue = operation(emulator, emulator.memory.getByte(zeroPageAddress));

    emulator.memory.setByte(zeroPageAddress, newValue);

    emulator.advanceExecutionPointer(2);

    return zeroPageResult;
}

function zeroPageX(emulator) {

    const zeroPageXAddress = addressing.getZeroPageXAddress(emulator);
    const newValue = operation(emulator, emulator.memory.getByte(zeroPageXAddress));

    emulator.memory.setByte(zeroPageXAddress, newValue);

    emulator.advanceExecutionPointer(2);

    return zeroPageXResult;
}

function absolute(emulator) {

    const absoluteAddress = addressing.getAbsoluteAddress(emulator);
    const newValue = operation(emulator, emulator.memory.getByte(absoluteAddress));

    emulator.memory.setByte(absoluteAddress, newValue);

    emulator.advanceExecutionPointer(3);

    return absoluteResult;
}

function absoluteX(emulator) {

    const absoluteXAddress = addressing.getAbsoluteXAddress(emulator).address;
    const newValue = operation(emulator, emulator.memory.getByte(absoluteXAddress));

    emulator.memory.setByte(absoluteXAddress, newValue);

    emulator.advanceExecutionPointer(3);

    return absoluteXResult;
}

exports.operation = operation;
exports.accumulator = accumulator;
exports.zeroPage = zeroPage;
exports.zeroPageX = zeroPageX;
exports.absolute = absolute;
exports.absoluteX = absoluteX;
exports.name = 'LSR';
