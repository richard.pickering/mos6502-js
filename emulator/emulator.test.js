
const emulatorModule = require("./emulator");

/* ===== Program counter ===== */

test("Emulator advances program counter by 1 as default", () => {

    const emulator = new emulatorModule.Emulator();
    expect(emulator.registers.programCounter).toBe(0x600);

    emulator.advanceExecutionPointer();

    expect(emulator.registers.programCounter).toBe(0x601);
});

test("Emulator can advance program counter by up to 3 bytes", () => {

    const emulator = new emulatorModule.Emulator();

    expect(emulator.registers.programCounter).toBe(0x600);

    emulator.advanceExecutionPointer(3);

    expect(emulator.registers.programCounter).toBe(0x603);
});

test("Program counter advances beyond 3 bytes fail", () => {

    const emulator = new emulatorModule.Emulator();

    expect(() => emulator.advanceExecutionPointer(4))
        .toThrowError("Cannot advance program counter by 4 as no MOS-6502 instruction has a width of more than 3 bytes.");
});

/* ===== Stack ===== */

test("Pushing to stack decrements the stack pointer and stores the value within the stack", () => {

    const emulator = new emulatorModule.Emulator();

    expect(emulator.registers.stackPointer).toBe(0xFF);

    emulator.pushToStack(13);

    expect(emulator.registers.stackPointer).toBe(0xFE);
    expect(emulator.memory.getByte(0x1FF)).toBe(13);
});

test("Pushing to the stack more than 255 times causes the stack pointer to wrap.", () => {

    const emulator = new emulatorModule.Emulator();

    expect(emulator.registers.stackPointer).toBe(0xFF);

    for (let i = 0; i <= 0xFF; i++) {
        emulator.pushToStack(1);
    }

    expect(emulator.registers.stackPointer).toBe(0xFF);
});

test("Pulling from the stack increments the stack pointer and returns the value stored at that memory location in the stack.", () => {

    const emulator = new emulatorModule.Emulator();

    expect(emulator.registers.stackPointer).toBe(0xFF);

    emulator.pushToStack(13);
    expect(emulator.pullFromStack()).toBe(13);
});

test("Pulling from the stack when the stack is empty causes the stack pointer to wrap.", () => {

    const emulator = new emulatorModule.Emulator();

    expect(emulator.registers.stackPointer).toBe(0xFF);

    emulator.pullFromStack();

    expect(emulator.registers.stackPointer).toBe(0);
});

/* ===== Emulator operation ===== */

test("Cannot start an emulator instance multiple times", () => {

    const emulator = new emulatorModule.Emulator();

    emulator.run();

    expect(() => emulator.run({})).toThrowError("Cannot start new MOS-6502 emulator - one is already running");
});
