
const emulator = require("../emulator");

/**
 * Runs the following program:
 * LDA #$10 - 2 cycles (zero page)
 * STA $20  - 3 cycles (zero page)
 * Expects that the  memory location 0x20 will contain 0x10 upon completion of the program.
 */
test("Loads the accumulator from the operation and stores the value back to memory.", () => {

    const instance = new emulator.Emulator();

    instance.loadProgram([0xA9, 0x10, 0x85, 0x20]);

    instance.run({ maxCycles: 5, blocking: true });

    expect(instance.registers.accumulator).toBe(0x10);
    expect(instance.memory.getByte(0x20)).toBe(0x10);
    expect(instance.executedTicks).toBe(5);
    expect(instance.skipCycles).toBe(0);
});

/**
 * Runs the following program (performing quick multiplication by 10):
 * Program from http://www.6502.org/source/integers/fastx10.htm
 * LDA #$02 - 2 cycles
 * ASL A    - 2 cycles
 * STA $EA  - 4 cycles (absolute)
 * ASL A    - 2 cycles
 * ASL A    - 2 cycles
 * CLC      - 2 cycles
 * ADC $EA  - 4 cycles (absolute)
 *    Total - 16 cycles
 */
test("Performs a quick multiplication by 10 program", () => {

    const instance = new emulator.Emulator();

    instance.loadProgram([0xA9, 0x02, 0x0A, 0x8D, 0xEA, 0x00, 0x0A, 0x0A, 0x18, 0x6D, 0xEA, 0x00]);

    instance.run({ maxCycles: 18, blocking: true });

    expect(instance.registers.accumulator).toBe(20);

    expect(instance.executedTicks).toBe(18);
    expect(instance.skipCycles).toBe(0);
});

/**
 * Runs the following program to test indexed indirect addressing works as expected:
 *
 * LDA #$10 (immediate mode - 2 cycles)
 * TAX      (2 cycles
 *
 * LDA #$01 (2 cycles)
 * STA $40  (4 cycles)
 *
 * LDA #$02 (2 cycles)
 * STA $41  (4 cycles)
 *
 * LDA #$FF (2 cycles)
 * STA ($30,X) (6 cycles)
 *
 * Overall 24 cycles
 *
 * Stores 0xFF at memory address 0x201
 */
test("Indexed indirect addressing works correctly", () => {

    const instance = new emulator.Emulator();

    instance.loadProgram([0xA9, 0x10, 0xAA, 0xA9, 0x01, 0x8D, 0x40, 0x00, 0xA9, 0x02, 0x8D, 0x41, 0x00, 0xA9, 0xFF, 0x81, 0x30]);

    instance.run({ maxCycles: 24, blocking: true });

    expect(instance.memory.getByte(0x201)).toBe(0xFF);

    expect(instance.executedTicks).toBe(24);
    expect(instance.skipCycles).toBe(0);
});

/* ===== BRANCHING ===== */

/**
 * Runs the following program to test branching if set functionality:
 * LDA #$30
 * STA $50
 *
 * LDA #$FF
 * ADC $50
 *
 * BCC CC
 * BCS CS
 *
 * CC:
 * LDA #$10
 * STA $1
 * JMP END
 *
 * CS:
 * LDA #$20
 * STA $1
 *
 * END:
 */
test("Branches correctly if the carry flag is set", () => {

    const instance = new emulator.Emulator();

    instance.loadProgram([
        0xA9, 0x30, // LDA #$30 - 2 cycles
        0x85, 0x50, // STA $50 - 3 cycles
        0xA9, 0xFF, // LDA #$FF - 2 cycles
        0x65, 0x50, // ADC $50 - 3 cycles
        0x90, 0x02, // BCC CC (label is at offset 2 from instruction) - 2 cycles
        0xB0, 0x07, // BCS CS (label is at offset 7 from instruction) - 3 cycles
        0xA9, 0x10, // LDA #$10 - 2 cycles
        0x85, 0x01, // STA $01 - 3 cycles
        0x4C, 0x17, 0x00, // JMP $0017 - 3 cycles
        0xA9, 0x20, // LDA #$20 - 2 cycles
        0x85, 0x01  // STA $1 - 3 cycles
    ]);

    instance.run({maxCycles: 20, blocking: true});

    expect(instance.memory.getByte(1)).toBe(0x20);
    expect(instance.skipCycles).toBe(0);
    expect(instance.executedTicks).toBe(20);
});

/**
 * Runs the following program to test branching if clear functionality:
 * LDA #$30
 * STA $50
 *
 * LDA #$10
 * ADC $50
 *
 * BCC CC
 * BCS CS
 *
 * CC:
 * LDA #$10
 * STA $1
 * JMP END
 *
 * CS:
 * LDA #$20
 * STA $1
 *
 * END:
 */
test("Branches correctly if the carry flag is clear", () => {

    const instance = new emulator.Emulator();

    instance.loadProgram([
        0xA9, 0x30, // LDA #$30 - 2 cycles
        0x85, 0x50, // STA $50 - 3 cycles
        0xA9, 0x10, // LDA #$10 - 2 cycles
        0x65, 0x50, // ADC $50 - 3 cycles
        0x90, 0x02, // BCC CC (label is at offset 2 from instruction) - 4 cycles
        0xB0, 0x07, // BCS CS (label is at offset 7 from instruction) - 2 cycles
        0xA9, 0x10, // LDA #$10 - 2 cycles
        0x85, 0x01, // STA $01 - 3 cycles
        0x4C, 0x17, 0x00, // JMP $0017 - 3 cycles
        0xA9, 0x20, // LDA #$20 - 2 cycles
        0x85, 0x01  // STA $1 - 3 cycles
    ]);

    instance.run({maxCycles: 21, blocking: true});

    expect(instance.memory.getByte(1)).toBe(0x10);
    expect(instance.skipCycles).toBe(0);
    expect(instance.executedTicks).toBe(21);
});

/**
 * Runs the following program to ensure branch not equal behaviour works as expected
 * LDA #$F0
 * STA $50
 *
 * LDA #$0F
 * AND $50
 *
 * BNE BC
 * BEQ BS
 *
 * BC:
 * LDA #$10
 * STA $1
 * JMP END
 *
 * BS:
 * LDA #$20
 * STA $1
 *
 * END:
 */
test("Branch not equal works as expected", () => {

    const instance = new emulator.Emulator();

    instance.loadProgram([
        0xA9, 0x30, // LDA #$F0 - 2 cycles
        0x85, 0x50, // STA $50 - 3 cycles
        0xA9, 0xFF, // LDA #$0F - 2 cycles
        0x25, 0x50, // AND $50 - 3 cycles
        0xD0, 0x02, // BNE (offset of 2 bytes) - 4 cycles
        0xF0, 0x07, // BEQ (offset of 7 bytes) - 2 cycles
        0xA9, 0x10, // LDA #$10 - 2 cycles
        0x85, 0x01, // STA $01 - 3 cycles
        0x4C, 0x17, 0x00, // JMP $0017 - 3 cycles
        0xA9, 0x20, // LDA #$20 - 2 cycles
        0x85, 0x01  // STA $01 - 3 cycles
    ]);

    instance.run({ maxCycles: 21, blocking: true });

    expect(instance.skipCycles).toBe(0);
    expect(instance.memory.getByte(1)).toBe(0x10);
    expect(instance.executedTicks).toBe(21);
});

/**
 * Runs the following program to ensure branch not equal behaviour works as expected
 * LDA #$F0
 * STA $50
 *
 * LDA #$0F
 * AND $50
 *
 * BNE bc
 * BEQ bs
 *
 * BC:
 * LDA #$10
 * STA $1
 * JMP END
 *
 * BS:
 * LDA #$20
 * STA $1
 *
 * END:
 */
test("Branch equal works as expected", () => {

    const instance = new emulator.Emulator();

    instance.loadProgram([
        0xA9, 0xF0, // LDA #$F0 - 2 cycles
        0x85, 0x50, // STA $50 - 3 cycles
        0xA9, 0x0F, // LDA #$0F - 2 cycles
        0x25, 0x50, // AND $50 - 3 cycles
        0xD0, 0x02, // BNE (offset of 2 bytes) - 4 cycles
        0xF0, 0x07, // BEQ (offset of 7 bytes) - 2 cycles
        0xA9, 0x10, // LDA #$10 - 2 cycles
        0x85, 0x01, // STA $01 - 3 cycles
        0x4C, 0x17, 0x00, // JMP $0017 - 3 cycles
        0xA9, 0x20, // LDA #$20 - 2 cycles
        0x85, 0x01  // STA $01 - 3 cycles
    ]);

    instance.run({ maxCycles: 20, blocking: true });

    expect(instance.skipCycles).toBe(0);
    expect(instance.memory.getByte(1)).toBe(0x20);
    expect(instance.executedTicks).toBe(20);
});

/* ===== STARTUP AND OPERATION ===== */

test("Runs programs asynchronously", done => {

    const instance = new emulator.Emulator();

    instance.loadProgram([0xA9, 0x10, 0x85, 0x20]);

    instance.run({ maxCycles: 5 });

    setTimeout(() => {

        expect(instance.registers.accumulator).toBe(0x10);
        expect(instance.memory.getByte(0x20)).toBe(0x10);
        expect(instance.executedTicks).toBe(5);
        expect(instance.skipCycles).toBe(0);

        done();
    }, 50);
});

