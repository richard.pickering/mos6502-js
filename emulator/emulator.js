
const registers = require("./registers/registers");
const memory = require('./memory');
const instructions = require("./instructions/instructions");

const STACK_MEMORY_START = 0x100;

class Emulator {

    constructor() {
        this.registers = new registers.RegistersState();
        this.memory = new memory.Memory();

        this.executedTicks = 0;
        this.skipCycles = 0;
    }

    get processorStatus() {
        return this.registers.processorStatus;
    }

    /**
     * Starts the emulation of the MOS-6502 CPU
     * Allowed options are:
     * - maxCycles = defines a number of cycles to run the emulation for and then stop the emulation.
     * - blocking = Whether to block or perform the execution of the emulation asynchronously. This requires a max cycles argument or is ignored
     * @param {Object} options
     */
    run(options) {

        let maxCycles = -1;
        let blocking = false;

        if (options !== undefined) {

            if (options.maxCycles) {
                maxCycles = options.maxCycles;
            }

            if (options.blocking) {
                blocking = options.blocking && maxCycles;
            }
        }

        if (this.runningInstance) {
            throw new Error("Cannot start new MOS-6502 emulator - one is already running");
        }

        if (!blocking) {
            this.runningInstance = setInterval(() => {
                this.tick();

                if (this.executedTicks === maxCycles) {
                    this.stop();
                }
            }, 1);
        } else {

            while (this.executedTicks < maxCycles) {

                this.tick();
            }
        }
    }

    stop() {
        clearInterval(this.runningInstance);
    }

    tick() {

        if (this.skipCycles === 0) {

            const opCode = this.memory.getByte(this.registers.programCounter);
            const instruction = instructions.getOperation(opCode);

            const result = instruction.operation(this);

            this.skipCycles = result.cycles - 1;

            console.debug(`Loaded instruction ${opCode.toString(16)} - cycles: ${result.cycles}`);

        } else {

            this.skipCycles--;
        }

        this.executedTicks++;
    }

    /**
     * Loads a program into memory as a list of bytes.
     * @param {Array} bytes
     */
    loadProgram(bytes) {
        this.memory.loadProgram(bytes);
    }

    advanceExecutionPointer(bytes) {

        if (bytes === undefined) {
            bytes = 1;
        }

        if (bytes > 3) {
            throw new Error(`Cannot advance program counter by ${bytes} as no MOS-6502 instruction has a width of more than 3 bytes.`);
        }

        this.registers.programCounter += bytes;
    }

    pushToStack(byte) {

        this.memory.setByte(STACK_MEMORY_START + this.registers.stackPointer, byte);

        this.registers.stackPointer--;

        if (this.registers.stackPointer < 0) {
            this.registers.resetStackPointer();
        }
    }

    pullFromStack() {

        this.registers.stackPointer++;

        const storedValue = this.memory.getByte(STACK_MEMORY_START + this.registers.stackPointer);

        if (this.registers.stackPointer > 0xFF) {
            this.registers.stackPointer = 0;
        }

        return storedValue;
    }
}

exports.Emulator = Emulator;
