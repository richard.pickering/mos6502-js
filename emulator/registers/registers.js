
const binaryUtils = require("../utils/binaryUtils");

class ProcessorStatus {

    constructor() {
        this.value = 0;
    }

    /**
     * Sets the carry flag
     * @param {boolean} value
     */
    set carry(value) {
        this.value = binaryUtils.setBit(this.value, 0, value);
    }

    /**
     * Gets the carry flag
     * @return {boolean}
     */
    get carry() {
        return binaryUtils.isBitSet(0, this.value);
    }

    /**
     * Sets the zero flag
     * @param {boolean} value
     */
    set zero(value) {
        this.value = binaryUtils.setBit(this.value, 1, value);
    }

    /**
     * Gets the zero flag which is b1 of the processor status value
     * @return {boolean}
     */
    get zero() {
        return binaryUtils.isBitSet(1, this.value);
    }

    /**
     * Sets interrupt disable flag
     * @param {boolean} value
     */
    set interruptDisable(value) {
        this.value = binaryUtils.setBit(this.value, 2, value);
    }

    /**
     * Gets the interrupt disable flag
     * @return {boolean}
     */
    get interruptDisable() {
        return binaryUtils.isBitSet(2, this.value);
    }

    /**
     * Sets the decimal flag
     * @param {boolean} value
     */
    set decimal(value) {
        this.value = binaryUtils.setBit(this.value, 3, value);
    }

    /**
     * Gets the decimal flag
     * @return {boolean}
     */
    get decimal() {
        return binaryUtils.isBitSet(3, this.value);
    }

    /**
     * Sets the break flag
     * @param {boolean} value
     */
    set break(value) {
        this.value = binaryUtils.setBit(this.value, 4, value);
    }

    /**
     * Gets the break flag
     * @return {boolean}
     */
    get break() {
        return binaryUtils.isBitSet(4, this.value);
    }

    /**
     * Sets the overflow flag
     * @param {boolean} value
     */
    set overflow(value) {
        this.value = binaryUtils.setBit(this.value, 6, value);
    }

    /**
     * Gets the overflow flag
     * @return {boolean}
     */
    get overflow() {
        return binaryUtils.isBitSet(6, this.value);
    }

    /**
     * Sets the negative flag
     * @param {boolean} value
     */
    set negative(value) {
        this.value = binaryUtils.setBit(this.value, 7, value);
    }

    /**
     * Gets the negative flag which is b7 of the Processor status value
     * @return {boolean}
     */
    get negative() {
        return binaryUtils.isBitSet(7, this.value);
    }

    /**
     * Determines the negative and zero status registers based on the value provided
     * @param {number} value
     */
    determineNegativeAndZero(value) {
        this.zero = value === 0;
        this.negative = binaryUtils.isBitSet(7, value);
    }
}

class RegistersState {

    constructor() {
        this.reset()
    }

    /**
     * Resets the registers to their default values.
     */
    reset() {
        this.programCounter = 0x600;
        this.stackPointer = 0xFF;
        this.accumulator = 0;
        this.x = 0;
        this.y = 0;
        this.processorStatus = new ProcessorStatus();
    }

    resetStackPointer() {
        this.stackPointer = 0xFF;
    }

    get programCounterHigh() {
        return this.programCounter >> 8;
    }

    set programCounterHigh(value) {
        this.programCounter = (value << 8) + this.programCounterLow;
    }

    get programCounterLow() {
        return binaryUtils.ensureXBits(this.programCounter, 8).clampedResult;
    }

    set programCounterLow(value) {
        this.programCounter = (this.programCounterHigh << 8) + value;
    }
}

exports.RegistersState = RegistersState;
exports.ProcessorStatus = ProcessorStatus;
