
const registers = require('./registers');

test('Registers are reset as expected', () => {

    const registersState = new registers.RegistersState();

    registersState.reset();

    expect(registersState.accumulator).toBe(0);
    expect(registersState.processorStatus).toEqual(new registers.ProcessorStatus());
    expect(registersState.programCounter).toBe(0x600);
    expect(registersState.stackPointer).toBe(0xFF);
    expect(registersState.x).toBe(0);
    expect(registersState.y).toBe(0);
});

test("resetStackPointer returns the stack pointer to its initial state (0xFF)", () => {

    const registersState = new registers.RegistersState();

    registersState.stackPointer = 0;

    registersState.resetStackPointer();

    expect(registersState.stackPointer).toBe(0xFF);
});

test("programCounterHigh is the most significant byte of the program counter", () => {

    const registersState = new registers.RegistersState();

    registersState.programCounter = 0xFF01;

    expect(registersState.programCounterHigh).toBe(0xFF);
});

test("programCounterHigh can be set without affecting the low byte", () => {

    const registersState = new registers.RegistersState();

    registersState.programCounter = 0xFF01;

    registersState.programCounterHigh = 0xFA;

    expect(registersState.programCounter).toBe(0xFA01);
});

test("programCounterLow is the least significant byte of the program counter", () => {

    const registersState = new registers.RegistersState();

    registersState.programCounter = 0xFF01;

    expect(registersState.programCounterLow).toBe(0x01);
});

test("programCounterLow can be set without affecting the high byte", () => {

    const registersState = new registers.RegistersState();

    registersState.programCounter = 0xFF01;

    registersState.programCounterLow = 0x02;

    expect(registersState.programCounter).toBe(0xFF02);
});

/* ===== Processor Status tests ==== */

test('Setting the carry flag does not affect other flags', () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.carry = true;

    expect(status.carry).toBeTruthy();
    expect(status.value).toBe(0b00000001);
});

test("Setting the zero flag does not affect other flags", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.zero = true;

    expect(status.zero).toBeTruthy();
    expect(status.value).toBe(0b00000010);
});

test("Setting the interrupt disable flag does not affect other flags", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.interruptDisable = true;

    expect(status.interruptDisable).toBeTruthy();
    expect(status.value).toBe(0b00000100);
});

test("Setting the decimal flag does not affect other flags", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.decimal = true;

    expect(status.decimal).toBeTruthy();
    expect(status.value).toBe(0b00001000);
});

test("Setting the break flag does not affect other flags", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.break = true;

    expect(status.break).toBeTruthy();
    expect(status.value).toBe(0b00010000);
});

test("Setting the overflow flag does not affect other flags", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.overflow = true;

    expect(status.overflow).toBeTruthy();
    expect(status.value).toBe(0b01000000);
});

test("Setting the negative flag does not affect other flags", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.negative = true;

    expect(status.negative).toBeTruthy();
    expect(status.value).toBe(0b10000000);
});

/* ===== Negative and zero status flag value determination ===== */

test("Determines negative status flag based on negative value", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.determineNegativeAndZero(-1);

    expect(status.negative).toBeTruthy();
    expect(status.zero).toBeFalsy();
});

test("Determines zero status flag based on zero value", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.determineNegativeAndZero(0);

    expect(status.negative).toBeFalsy();
    expect(status.zero).toBeTruthy();
});

test("Does not modify status flags if value is positive", () => {

    const status = new registers.ProcessorStatus();

    expect(status.value).toBe(0);

    status.determineNegativeAndZero(10);

    expect(status.value).toBe(0);
});

test("Resets the zero and negative flags even if not negative or zero", () => {
    const status = new registers.ProcessorStatus();

    status.value = 0b11111111;

    status.determineNegativeAndZero(10);

    expect(status.value).toBe(0b01111101);
});
