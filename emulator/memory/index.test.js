
const memoryModule = require("./index");

let memory = new memoryModule.Memory();

beforeEach(() => {
    memory.reset();
});

/* ===== Reset memory tests ===== */

test("resetMemory resets all values in memory to zero", () => {

    memory.setByte(1, 100);

    memory.reset();

    const allMemory = memory.slice();

    const expectedMemory = new ArrayBuffer(0x10000);
    const expectedMemoryView = new Uint8Array(expectedMemory);
    expectedMemoryView.forEach((value, index) => expectedMemoryView[index] = 0);

    expect(allMemory).toEqual(expectedMemoryView);
});

/* ===== setByte tests ===== */

test("setByte sets a byte at an offset in memory", () => {

    expect(memory.slice(1, 2)[0]).toEqual(0);

    memory.setByte(1, 128);

    expect(memory.slice(1, 2)[0]).toEqual(128);
});

test("setByte fails if index < 0", () => {
    expect(() => memory.setByte(-1)).toThrowError("Could not set value 'undefined' at index '-1' as index was below 0");
});

test("setByte fails if index >= max memory", () => {
    expect(() => memory.setByte(0x10000, 1))
        .toThrowError("Could not set value '1' at index '65536' as the index was greater than the size of memory ('65536' bytes)");
});

/* ===== getByte tests ===== */

test("getByte gets the byte at the specified offset in memory", () => {

    expect(memory.getByte(1)).toBe(0);

    memory.setByte(1, 128);

    expect(memory.getByte(1)).toBe(128);
});

test("getByte fails if index < 0", () => {
    expect(() => memory.getByte(-1)).toThrowError("Could not get byte at '-1' - index is < 0");
});

test("getByte fails if index >= max memory", () => {
    expect(() => memory.getByte(0x10000)).toThrowError("Could not get byte at '65536' - index is > max memory size ('65536' bytes)");
});

/* ===== getShort tests ===== */

test("Successfully provides a short at the specified address", () => {

    memory.setByte(10, 0x80);
    memory.setByte(11, 0x30);

    expect(memory.getShort(10)).toBe(0x3080);
});

/* ===== slice tests ===== */

test("slice retrieves a slice of memory between offsets", () => {

    memory.setByte(1, 10);
    memory.setByte(2, 11);
    memory.setByte(3, 12);

    expect(memory.slice(1, 4)).toEqual(new Uint8Array([10, 11, 12]));
});

test("slice fails if startIndex >= endIndex", () => {
    expect(() => memory.slice(10, 1))
        .toThrowError("Could not get memory between 10 and 1 - 10 >= 1");
});

test("slice fails if startIndex < 0", () => {
    expect(() => memory.slice(-1))
        .toThrowError("Could not get memory between -1 and undefined - -1 < 0");
});

test("slice fails if startIndex >= max memory", () => {
    expect(() => memory.slice(0x10000, 0))
        .toThrowError("Could not get memory between 65536 and 0 - 65536 >= max memory (65536)");
});

test("slice fails if endIndex >= max memory", () => {
    expect(() => memory.slice(0, 0x10000))
        .toThrowError("Could not get memory between 0 and 65536 - 65536 >= max memory (65536)");
});

test("slice fails if endIndex < 0", () => {
    expect(() => memory.slice(0, -1))
        .toThrowError("Could not get memory between 0 and -1 - -1 < 0");
});

/* ===== Load program ===== */

test("Loads a program's bytes into memory starting at the default program space offset (0x600)", () => {
    memory.loadProgram([0x01, 0x02, 0x03]);

    expect(memory.getByte(0x600)).toBe(0x01);
    expect(memory.getByte(0x601)).toBe(0x02);
    expect(memory.getByte(0x602)).toBe(0x03);
});

/* ===== Load ROM ===== */

test("Loads a ROM into memory from the specified offset", () => {
    memory.loadRom([0x01, 0x02, 0x03], 10);

    expect(memory.getByte(10)).toBe(0x01);
    expect(memory.getByte(11)).toBe(0x02);
    expect(memory.getByte(12)).toBe(0x03);
});

test("If the offset + length of the ROM exceeds max memory, an error is thrown", () => {

    expect(() => memory.loadRom([0x01, 0x02, 0x03], 0x10000))
        .toThrowError("Could not load ROM as its byte length (3) + offset (65536) exceeds max memory (65536)");
});
