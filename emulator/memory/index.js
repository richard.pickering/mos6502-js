
const MEMORY_SIZE = 0x10000;
const PROGRAM_SPACE_START = 0x600;

class Memory {

    constructor() {
        let memory = new ArrayBuffer(MEMORY_SIZE);
        this.view = new Uint8Array(memory);
    }

    /**
     * Sets a specific byte value within memory
     * @param index
     * @param value
     * @throws Error if index < 0 or index >= max memory size
     */
    setByte(index, value) {

        if (index < 0) {
            throw new Error(`Could not set value '${value}' at index '${index}' as index was below 0`);
        }

        if (index >= MEMORY_SIZE) {
            throw new Error(`Could not set value '${value}' at index '${index}' as the index was greater than the size of memory ('${MEMORY_SIZE}' bytes)`)
        }

        this.view[index] = value;
    }

    /**
     * Gets the byte at a specified index within the memory buffer.
     * @param index
     * @return {number}
     * @throws Error if index < 0 or index >= max memory size
     */
    getByte(index) {

        if (index < 0) {
            throw new Error(`Could not get byte at '${index}' - index is < 0`);
        }

        if (index >= MEMORY_SIZE) {
            throw new Error(`Could not get byte at '${index}' - index is > max memory size ('${MEMORY_SIZE}' bytes)`);
        }

        return this.view[index];
    }

    /**
     * Gets a short at the specified index within the memory buffer.
     *
     * @param index
     * @return {number}
     * @throws Error if index < 0 or index >= max memory size or if index + 1 >= max memory size
     */
    getShort(index) {
        return (this.getByte(index + 1) << 8) + this.getByte(index);
    }

    /**
     * Resets memory to default values of zero.
     */
    reset() {
        this.view.forEach((value, index) => this.view[index] = 0);
    }

    /**
     * Returns a slice of the memory between the start index and the end index.
     * @param startIndex
     * @param endIndex
     * @return {Uint8Array}
     */
    slice(startIndex, endIndex) {

        if (startIndex < 0) {
            throw new Error(`Could not get memory between ${startIndex} and ${endIndex} - ${startIndex} < 0`);
        }

        if (startIndex >= MEMORY_SIZE) {
            throw new Error(`Could not get memory between ${startIndex} and ${endIndex} - ${startIndex} >= max memory (${MEMORY_SIZE})`);
        }

        if (endIndex < 0) {
            throw new Error(`Could not get memory between ${startIndex} and ${endIndex} - ${endIndex} < 0`);
        }

        if (endIndex >= MEMORY_SIZE) {
            throw new Error(`Could not get memory between ${startIndex} and ${endIndex} - ${endIndex} >= max memory (${MEMORY_SIZE})`);
        }

        if (startIndex >= endIndex) {
            throw new Error(`Could not get memory between ${startIndex} and ${endIndex} - ${startIndex} >= ${endIndex}`);
        }

        if (startIndex === undefined && endIndex === undefined) {
            return this.view;
        }

        return this.view.slice(startIndex, endIndex);
    }

    /**
     * Loads the provided program by setting the bytes offset from the start of program space.
     * @param {Array} bytes
     */
    loadProgram(bytes) {

        for (let i = 0; i < bytes.length; i++) {
            this.setByte(PROGRAM_SPACE_START + i, bytes[i]);
        }
    }

    /**
     * Loads a ROM into memory starting at the specified offset.
     * If the ROM is too big to fit into memory from the specified offset, an error will be thrown.
     * @param bytes
     * @param {number} offset
     */
    loadRom(bytes, offset) {

        if (bytes.length + offset >= this.view.length) {
            throw new Error(`Could not load ROM as its byte length (${bytes.length}) + offset (${offset}) exceeds max memory (${MEMORY_SIZE})`);
        }

        for (let i = 0; i < bytes.length; i++) {
            this.setByte(offset + i, bytes[i]);
        }
    }
}

exports.Memory = Memory;
