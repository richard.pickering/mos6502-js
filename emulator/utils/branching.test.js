
const branching = require("./branching");
const memory = require("../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        registers: {
            programCounter: 0
        },
        memory: new memory.Memory()
    };
});

test("Branching adds a relative offset to the program counter", () => {

    emulator.memory.setByte(1, 32);

    expect(branching.branch(emulator)).toEqual({ pageCrossed: false });

    expect(emulator.registers.programCounter).toBe(34);
});

test("Branching with a negative offset move the program counter backwards", () => {

    emulator.registers.programCounter = 131;
    emulator.memory.setByte(132, 128);

    expect(branching.branch(emulator)).toEqual({ pageCrossed: false });

    expect(emulator.registers.programCounter).toBe(5);
});

test("Branching across memory pages is accurately reflected in the return value of the branch function", () => {

    emulator.registers.programCounter = 129;
    emulator.memory.setByte(130, 127);

    expect(branching.branch(emulator)).toEqual({ pageCrossed: true });

    expect(emulator.registers.programCounter).toBe(258);
});

test("Branching above 0xFFFF wraps the program counter value", () => {

    emulator.registers.programCounter = 0xFFFE;
    emulator.memory.setByte(0xFFFF, 3);

    expect(branching.branch(emulator)).toEqual({ pageCrossed: true });

    expect(emulator.registers.programCounter).toBe(4);
});

test("Branching below 0 wraps the program counter value", () => {

    emulator.registers.programCounter = 10;
    emulator.memory.setByte(11, 243);

    expect(branching.branch(emulator)).toEqual({ pageCrossed: true });

    expect(emulator.registers.programCounter).toBe(0xFFFE);
});
