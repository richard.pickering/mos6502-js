const addressing = require("./addressing");
const memoryModule = require("../memory");

let emulator = {};

beforeEach(() => {
    emulator = {
        memory: new memoryModule.Memory(),
        registers: {
            programCounter: 0,
            x: 0
        }
    };
});

/* ===== Memory page number ===== */

test("0xFF is on memory page 1", () => {
    expect(addressing.getAddressPageNumber(0xFE)).toBe(0);
    expect(addressing.getAddressPageNumber(0x100)).toBe(1);
});

/* ===== Zero page addressing ===== */

test("Successfully retrieves a zero page address", () => {

    emulator.memory.setByte(1, 128);

    expect(addressing.getZeroPageAddress(emulator)).toBe(128);
});

/* ===== Zero page X addressing ===== */

test("Retrieves a zero page X address", () => {

    emulator.memory.setByte(1, 0x0F);

    emulator.registers.x = 0x80;

    expect(addressing.getZeroPageXAddress(emulator)).toBe(0x8F);
});

test("Zero page X addresses exceeding 0xFF wrap", () => {

    emulator.memory.setByte(1, 0xFF);

    emulator.registers.x = 0x80;

    expect(addressing.getZeroPageXAddress(emulator)).toBe(0x7F);
});

/* ===== Zero page Y addressing ===== */

test("Retrieves a zero page Y address", () => {

    emulator.memory.setByte(1, 0x0F);

    emulator.registers.y = 0x80;

    expect(addressing.getZeroPageYAddress(emulator)).toBe(0x8F);
});

test("Zero page Y addresses exceeding 0xFF wrap", () => {

    emulator.memory.setByte(1, 0xFF);

    emulator.registers.y = 0x80;

    expect(addressing.getZeroPageYAddress(emulator)).toBe(0x7F);
});

/* ===== Absolute addressing ===== */

test("Absolute addressing works", () => {

    emulator.memory.setByte(1, 0x30);
    emulator.memory.setByte(2, 0x80);

    expect(addressing.getAbsoluteAddress(emulator)).toBe(0x8030);
});

/* ===== Absolute X addressing ===== */

test("Absolute X addressing works", () => {

    emulator.memory.setByte(1, 0x10);
    emulator.memory.setByte(2, 0x80);

    emulator.registers.x = 0x10;

    expect(addressing.getAbsoluteXAddress(emulator)).toEqual({ address: 0x8020, pageCrossed: false });
});

test("Absolute X addressing specifies if memory pages have been crossed as a result of adding X register value to base address", () => {

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x00);

    emulator.registers.x = 0x01;

    expect(addressing.getAbsoluteXAddress(emulator)).toEqual({ address: 0x100, pageCrossed: true });
});

/* ===== Absolute Y addressing ===== */

test("Absolute Y addressing works", () => {

    emulator.memory.setByte(1, 0x30);
    emulator.memory.setByte(2, 0x80);

    emulator.registers.y = 0x10;

    expect(addressing.getAbsoluteYAddress(emulator)).toEqual({
        address: 0x8040,
        pageCrossed: false
    });
});

test("Absolute Y addressing specifies if memory pages have been crossed as a result of adding Y register value to base address", () => {

    emulator.memory.setByte(1, 0xFF);
    emulator.memory.setByte(2, 0x00);

    emulator.registers.y = 0x01;

    expect(addressing.getAbsoluteYAddress(emulator)).toEqual({
        address: 0x100,
        pageCrossed: true
    });
});

/* ===== Indexed indirect addressing ===== */

test("Indexed indirect addressing works", () => {

    emulator.memory.setByte(1, 0x30);

    emulator.memory.setByte(0x40, 0x01);
    emulator.memory.setByte(0x41, 0x02);

    emulator.registers.x = 0x10;

    const address = addressing.getIndexedIndirectAddress(emulator);

    expect(address).toBe(0x201);
});

test("Indexed indirect addressing wraps on zero page if address exceeds zero page length", () => {

    emulator.memory.setByte(1, 0x06);

    emulator.memory.setByte(0x05, 0x01);
    emulator.memory.setByte(0x06, 0x02);

    emulator.registers.x = 0xFF;

    const address = addressing.getIndexedIndirectAddress(emulator);

    expect(address).toBe(0x201);
});

/* ===== Indirect indexed addressing ===== */

test("Indirect indexed addressing works", () => {

    emulator.registers.y = 0x01;

    emulator.memory.setByte(1, 0x40);

    emulator.memory.setByte(0x40, 0x01);
    emulator.memory.setByte(0x41, 0x02);

    emulator.memory.setByte(0x202, 0xFF);

    const addressingResult = addressing.getIndirectIndexedAddress(emulator);

    expect(addressingResult).toEqual({ address: 0x202, pageCrossed: false });
});

test("Indirect indexed addressing indicates whether a memory page has been crossed when applying the Y register to the address", () => {

    emulator.registers.y = 0xFF;

    emulator.memory.setByte(1, 0x40);

    emulator.memory.setByte(0x40, 0x01);
    emulator.memory.setByte(0x41, 0x02);

    emulator.memory.setByte(0x300, 0xFF);

    const addressingResult = addressing.getIndirectIndexedAddress(emulator);

    expect(addressingResult).toEqual({ address: 0x300, pageCrossed: true });
});

/* ===== Indirect addressing ===== */

test("Indirect addressing functions", () => {

    emulator.memory.setByte(1, 0x20);
    emulator.memory.setByte(2, 0x01);

    emulator.memory.setByte(0x0120, 0xFC);
    emulator.memory.setByte(0x0121, 0xBA);

    expect(addressing.getIndirectAddress(emulator)).toBe(0xBAFC);
});
