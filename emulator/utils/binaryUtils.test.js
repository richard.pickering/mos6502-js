
const utils = require('./binaryUtils.js');

/* ===== maskForBit tests ===== */

test('The bit mask for bit 4 is 8', () => {
    expect(utils.maskForBit(3)).toBe(8);
});

/* ===== maskForBits tests ===== */

test('The bit mask for 8 bits is 255', () => {
    expect(utils.maskForBits(8)).toBe(255);
});

/* ===== ensureXBits tests ===== */

test('1 00000000 is clamped to 0 when clamping using 8 bits with a remainder of 1', () => {

    const results = utils.ensureXBits(256, 8);

    expect(results.clampedResult).toBe(0);
    expect(results.leftOver).toBe(1);
});

test('11110000 is clamped to 48 (110000) using 6 bits with a remainder of 3', () => {

    const results = utils.ensureXBits(0b11110000, 6);

    expect(results.clampedResult).toBe(48);
    expect(results.leftOver).toBe(3);
});


/* ==== setBit ==== */

test("Sets the correct bit within a number, leaving all other bits unaffected", () => {

    const originalValue = 0b0100110;
    const newValue = utils.setBit(originalValue, 3, true);

    expect(newValue).toBe(0b0101110);
});

test("Un-sets the correct bit within a number, leaving all other bits unaffected", () => {

    const originalValue = 0b0101110;
    const newValue = utils.setBit(originalValue, 3, false);

    expect(newValue).toBe(0b0100110);
});

/* ===== isBitSet ===== */

test("isBitSet correctly specifies when a bit at a specified offset is set or not", () => {

    expect(utils.isBitSet(3, 0b1000)).toBeTruthy();
    expect(utils.isBitSet(3, 0b0111)).toBeFalsy();
});

/* ===== Convert to signed ===== */

test("Convert to signed correctly converts a positive number", () => {
    expect(utils.convertToSigned(32)).toBe(32);
});

test("Convert to signed correctly converts a negative number", () => {
    expect(utils.convertToSigned(128)).toBe(-128);
    expect(utils.convertToSigned(255)).toBe(-1);
});
