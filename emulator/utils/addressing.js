
function getAddressPageNumber(address) {
    return Math.trunc(address / 256);
}

function getImmediateValue(emulator) {
    return emulator.memory.getByte(emulator.registers.programCounter + 1);
}

function getZeroPageOperationPayload(emulator) {
    return emulator.memory.getByte(emulator.registers.programCounter + 1);
}

/**
 * Retrieves the address referenced by zero page addressing on the operation.
 * @param {Emulator} emulator
 */
function getZeroPageAddress(emulator) {
    return getZeroPageOperationPayload(emulator);
}

/**
 * Retrieves the address referenced by zero page X addressing on the operation.
 * Adds the base address, stored after the op-code, to the X register value.
 * Note: the value wraps if exceeds 0xFF.
 * @param emulator
 * @return {number}
 */
function getZeroPageXAddress(emulator) {

    const baseAddress = getZeroPageOperationPayload(emulator);
    const xValue = emulator.registers.x;

    return (baseAddress + xValue) % 0x100;
}

/**
 * Retrieves the address referenced by zero page Y addressing on the operation.
 * Adds the base address, stored after the op-code, to the Y register value.
 * Note: the value wraps if exceeds 0xFF.
 * @param emulator
 * @return {number}
 */
function getZeroPageYAddress(emulator) {

    const baseAddress = getZeroPageOperationPayload(emulator);
    const yValue = emulator.registers.y;

    return (baseAddress + yValue) % 0x100;
}

/**
 * Gets the absolute address offset from the program counter.
 * @param emulator
 * @return {number}
 */
function getAbsoluteAddress(emulator) {
    return emulator.memory.getShort(emulator.registers.programCounter + 1);
}

/**
 * Gets the absolute address offset from the program counter (operation payload) + the contents of the X register.
 * @param emulator
 * @return {number}
 */
function getAbsoluteXAddress(emulator) {

    const baseAddress = emulator.memory.getShort(emulator.registers.programCounter + 1);

    const finalAddress = baseAddress + emulator.registers.x;
    const pageCrossed = getAddressPageNumber(baseAddress) !== getAddressPageNumber(finalAddress);

    return {
        address: finalAddress,
        pageCrossed: pageCrossed
    };
}

/**
 * Gets the absolute address offset from the program counter (operation payload) + the contents of the Y register.
 * @param emulator
 */
function getAbsoluteYAddress(emulator) {

    const baseAddress = emulator.memory.getShort(emulator.registers.programCounter + 1);

    const finalAddress = baseAddress + emulator.registers.y;
    const pageCrossed = getAddressPageNumber(baseAddress) !== getAddressPageNumber(finalAddress);

    return {
        address: finalAddress,
        pageCrossed: pageCrossed
    };
}

/**
 * Gets the indexed indirect address (zero page table of addresses accessed by operation payload, added to X register
 * (with zero page wrap around) to give the location of the least significant byte of the target address)
 * @param emulator
 */
function getIndexedIndirectAddress(emulator) {

    const tableAddress = emulator.memory.getByte(emulator.registers.programCounter + 1);
    const tableOffset = emulator.registers.x;

    const baseAddress = (tableAddress + tableOffset) % 0x100;

    return emulator.memory.getShort(baseAddress);
}

/**
 * Gets the indirect indexed address (zero page location of least significant byte of target address is contained
 * in the operation payload, the full base address is found from memory (LSB address and address after)
 * and added to the Y register to give the full target address).
 * @param emulator
 */
function getIndirectIndexedAddress(emulator) {

    const lsbAddress = emulator.memory.getByte(emulator.registers.programCounter + 1);

    const baseAddress = emulator.memory.getShort(lsbAddress);
    const finalAddress = baseAddress + emulator.registers.y;

    const pageCrossed = getAddressPageNumber(baseAddress) !== getAddressPageNumber(finalAddress);

    return {
        address: finalAddress,
        pageCrossed: pageCrossed
    };
}

/**
 * Gets the 'indirect' address for the operation.  The JMP operation is the only operation to use this form of addressing.
 * @param emulator
 * @return {number}
 */
function getIndirectAddress(emulator) {

    const lsbAddress = emulator.memory.getShort(emulator.registers.programCounter + 1);

    return emulator.memory.getShort(lsbAddress);
}

exports.getImmediateValue = getImmediateValue;
exports.getZeroPageAddress = getZeroPageAddress;
exports.getZeroPageXAddress = getZeroPageXAddress;
exports.getZeroPageYAddress = getZeroPageYAddress;
exports.getAbsoluteAddress = getAbsoluteAddress;
exports.getAbsoluteXAddress = getAbsoluteXAddress;
exports.getAbsoluteYAddress = getAbsoluteYAddress;
exports.getIndexedIndirectAddress = getIndexedIndirectAddress;
exports.getIndirectIndexedAddress = getIndirectIndexedAddress;
exports.getAddressPageNumber = getAddressPageNumber;
exports.getIndirectAddress = getIndirectAddress;
