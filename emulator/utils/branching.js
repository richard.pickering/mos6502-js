
const addressing = require("./addressing");
const binaryUtils = require("./binaryUtils");

/**
 * Causes the emulator to branch to the relative offset a byte after the program counter
 * @param emulator
 */
function branch(emulator) {

    let relativeOffset = addressing.getImmediateValue(emulator);

    relativeOffset = binaryUtils.convertToSigned(relativeOffset);

    const oldProgramCounter = emulator.registers.programCounter + 1;

    emulator.registers.programCounter = ((oldProgramCounter + relativeOffset) + 1) % 0xFFFF;

    if (emulator.registers.programCounter < 0) {
        emulator.registers.programCounter += 0xFFFF;
    }

    const pageCrossed = addressing.getAddressPageNumber(oldProgramCounter) !== addressing.getAddressPageNumber(emulator.registers.programCounter);

    return {
        pageCrossed: pageCrossed
    };
}

exports.branch = branch;
