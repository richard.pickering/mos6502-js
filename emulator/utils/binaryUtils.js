

/**
 * Generates a bit mask required to get the value of a single bit
 * @param bit
 */
function maskForBit(bit) {
    return 1 << bit;
}


/**
 * Creates a bit mask which contains the right-most number of bits
 * @param bits
 * @return {number}
 */
function maskForBits(bits) {
    let bitMask = 0;
    let currentFactor = 1;

    for (let i = 0; i < bits; i++) {
        bitMask += currentFactor;
        currentFactor *= 2;
    }

    return bitMask;
}


/**
 * Ensures that the provided value fits within the parameterised number of bits
 * Returns details of the operation - i.e. if any data had to be removed.
 * @param value - The value to clamp to X bit
 * @param bits - the number of bits to clamp to
 * @return {{leftOver: number, clampedResult: number}}
 */
function ensureXBits(value, bits) {

    const bitMask = maskForBits(bits);

    return {
        leftOver: (value & (~bitMask)) >>> bits,
        clampedResult: value & bitMask
    };
}


/**
 * Sets the value of a single bit within a number.
 * @param {number} existingValue
 * @param {number} bit
 * @param {boolean} toSet
 * @return {number}
 */
function setBit(existingValue, bit, toSet) {

    const bitMask = maskForBit(bit);

    if (toSet) {
        return existingValue | bitMask;
    }

    return existingValue & (~bitMask);
}

/**
 * Specifies whether the bit at the specified offset is set or not (i.e. is 1).
 * @param {number} bit
 * @param {number} value
 * @return {boolean}
 */
function isBitSet(bit, value) {

    const bitMask = maskForBit(bit);

    return (value & bitMask) !== 0;
}

/**
 * Converts a 32 bit value to act like a signed byte (as it is temporarily stored in a single element Int8Array).
 * @param {number} value
 * @return {number} result
 */
function convertToSigned(value) {

    const arr = new Int8Array(1);

    arr[0] = value;

    return arr[0];
}

exports.maskForBit = maskForBit;
exports.maskForBits = maskForBits;
exports.ensureXBits = ensureXBits;
exports.setBit = setBit;
exports.isBitSet = isBitSet;
exports.convertToSigned = convertToSigned;
